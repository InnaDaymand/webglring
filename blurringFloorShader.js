/*
Inna Daymand, 2018, GaazIT

blurringFloorShader

input texture: postprocessing.rtReflexObjectToFloor.texture

make blurring on floor
*/

blurringFloorShader = {

	uniforms: {
    "resolution": {
      "name": "resolution",
      "type": "v2",
      "glslType": "vec2",
      "description": ""
    },
    "lightPosition": {
      "name": "lightPosition",
      "type": "v3",
      "glslType": "vec3",
      "description": ""
    },
	"uBasis" : {
		"name" :"uBasis",
		"value" : null
	},
	},
	vertexShader: [
"precision highp float;",
"precision highp int;",

"uniform mat4 modelMatrix;", //= object.MatrixWorld
"uniform mat4 modelViewMatrix;", // = camera.matrixWorldInverse*object.matrixWorld
"uniform mat4 projectionMatrix;", // = camera.projectionMatrix
"uniform mat4 viewMatrix;", // = camera.matrixWorldInverse
"uniform mat3 normalMatrix;", // = inverse transpose of modelViewMatrix
"uniform vec3 cameraPosition;", // = camera position in world space

"attribute vec3 position;",
"attribute vec3 normal;",
"attribute float shape;",
"attribute float isContour;",
"attribute float isBorder;",
"attribute float isBorder1;",

"uniform vec3 lightPosition;",
"uniform vec2 resolution;",


"varying vec3 vWorldPosition;",
"varying vec3 vWorldNormal;",
"varying vec2 vUv;",
"varying float vShape;",
"varying vec2 vtex_coord;",
"varying float vIsContour;",
"varying float vIsBorder;",
"varying float vIsBorder1;",

"void main() {",

    // This sets the position of the vertex in 3d space. The correct math is
    // provided below to take into account camera and object data.
"	vec4 mv_position=modelViewMatrix * vec4( position, 1.0 );",
"	vec4 worldPosition = modelMatrix * vec4( position, 1.0 );",
"	vWorldPosition =worldPosition.xyz; ",

"	vWorldNormal = normalize( mat3( modelMatrix[0].xyz, modelMatrix[1].xyz, modelMatrix[2].xyz ) * normal );",

"	 vIsContour = isContour;",     
"	 vIsBorder = isBorder;",     
"	 vIsBorder1 = isBorder1;",     

"   gl_Position = projectionMatrix * mv_position;",
// 	getting UV coordinates of position in input texture
"	float x1 = ((gl_Position.x/gl_Position.w)+1.0)*resolution.x/2.0;",
"	float y1 = ((gl_Position.y/gl_Position.w)+1.0)*resolution.y/2.0;",
"	vUv.x = (x1)/resolution.x;",
"	vUv.y = (y1)/resolution.y;",
//-------------------------------------------------------------
"	vShape=shape;",
"}"

	].join( "\n" ),

	fragmentShader: [
"precision highp float;",

	
"uniform sampler2D uBasis;",
"uniform vec2 resolution;",


"varying vec3 vWorldPosition;",
"varying vec3 vWorldNormal;",
"varying vec2 vUv;",
"varying float vShape;",
"varying float vIsContour;",
"varying float vIsBorder;",
"varying float vIsBorder1;",

//calculation of model for blurring
//apperture 5 left, 5 right (radius)
"float normColor(vec3 color){",
"	return (color.r+color.g+color.b);",
"}",
"const int radius=5;",
//calculation value of factor in row
"float calcX(int y,  float offset_x){",
"	vec3 color=texture2D(uBasis, vUv).rgb;",
"	vec2 onePixel=vec2(1.0, 1.0)/resolution;",
"	float avgNorm=normColor(color);",
"	float count=1.0;",
"	for(int x= -1; x > -radius -1 ; x--){",
"		color=texture2D(uBasis, vec2(vUv.x + offset_x*onePixel.x, vUv.y) + onePixel*vec2(x, y)).rgb;	",
"		avgNorm=avgNorm+normColor(color);",
"		count+=1.0;",
"	}",
"	for(int x= 1; x <radius +1 ; x++){",
"		color=texture2D(uBasis, vec2(vUv.x + offset_x*onePixel.x, vUv.y) + onePixel*vec2(x, y)).rgb;	",
"		avgNorm=avgNorm+normColor(color);",
"		count+=1.0;",
"	}",
"	avgNorm=avgNorm/count;",
"	return avgNorm;",
"}",

//calculation value of factor in col
"float calcColX(int x,  float offset_y){",
"	vec3 color=texture2D(uBasis, vUv).rgb;",
"	vec2 onePixel=vec2(1.0, 1.0)/resolution;",
"	float avgNorm=normColor(color);",
"	float count=1.0;",
"	for(int y= -1; y > -radius -1 ; y--){",
"		color=texture2D(uBasis, vec2(vUv.x, vUv.y+ offset_y*onePixel.y) + onePixel*vec2(x, y)).rgb;	",
"		avgNorm=avgNorm+normColor(color);",
"		count+=1.0;",
"	}",
"	for(int y= 1; y <radius +1 ; y++){",
"		color=texture2D(uBasis, vec2(vUv.x, vUv.y+ offset_y*onePixel.y) + onePixel*vec2(x, y)).rgb;	",
"		avgNorm=avgNorm+normColor(color);",
"		count+=1.0;",
"	}",
"	avgNorm=avgNorm/count;",
"	return avgNorm;",
"}",

//normalizing
"float normFactorColor(float normColor){",
"	return ((2.0*normColor - 3.5)/2.5); 	",// 0.5 ---- 3.0
"}",
"",

//set new color by finally
"vec3 setNewColor(vec3 inout_color, float norm){",
"	if(norm >=2.99){",
"		inout_color.r =1.0;",
"		inout_color.g =1.0;",
"		inout_color.b =1.0;",
"		return inout_color;",
"	}",
"	float oldNorm=normColor(inout_color);",
"	if((oldNorm) == 0.0)",
"		return vec3(0.0);",
"	float newRED=(inout_color.r*norm)/(oldNorm);",
"	float newGREEN=(inout_color.g*norm)/(oldNorm);",
"	float newBLUE=(inout_color.b*norm)/(oldNorm);",
"	newRED=newRED>1.0?1.0:newRED;",
"	newGREEN=newGREEN>1.0?1.0:newGREEN;",
"	newBLUE=newBLUE>1.0?1.0:newBLUE;",
"	inout_color.r=newRED;",
"	inout_color.g=newGREEN;",
"	inout_color.b=newBLUE;",
"	return inout_color;",
"}",
	
/*
	Y=1.95514+0.15639*X1+0.41018*X2+0.20514*X3+0.21475*X4+0.32244*X5+0.09775*X6-0.02928*X1*X6-0.02928*X2*X4-0.05869*X2*X5-
	0.04885*X2*X6-0.03908*X3*X4-0.04881*X3*X5+0.04877*X4*X6-0.02932*X1*X2*X5-
	0.02943*X1*X4*X5-0.03916*X2*X3*X5-0.03908*X2*X4*X6-0.06826*X2*X5*X6-0.02928*X3*X4*X6+0.14646*X4*X5*X6							
*/
//calculation of factors, and calculation of models for blurring		
"vec3 applyMyFilter(){",
"	vec3 color=texture2D(uBasis, vUv).rgb;",
"	float norm=normColor(color);",
"	float x1_f = calcX(-1, 0.0);",
"	float x2_f = calcX(0,  0.0);",
"	float x3_f = calcX(1,  0.0);",
"	float x4_f = calcColX(-1,  0.0);",
"	float x5_f = calcColX(0,  0.0);",
"	float x6_f = calcColX(1,  0.0);",
"	float X1=normFactorColor(x1_f);",
"	float X2=normFactorColor(x2_f);",
"	float X3=normFactorColor(x3_f);",
"	float X4=normFactorColor(x4_f);",
"	float X5=normFactorColor(x5_f);",
"	float X6=normFactorColor(x6_f);",
"	float y = 1.95514+0.15639*X1+0.41018*X2+0.20514*X3+0.21475*X4+0.32244*X5+0.09775*X6-0.02928*X1*X6-0.02928*X2*X4-0.05869*X2*X5-",
"	0.04885*X2*X6-0.03908*X3*X4-0.04881*X3*X5+0.04877*X4*X6-0.02932*X1*X2*X5-",
"	0.02943*X1*X4*X5-0.03916*X2*X3*X5-0.03908*X2*X4*X6-0.06826*X2*X5*X6-0.02928*X3*X4*X6+0.14646*X4*X5*X6;",
"	y = y >=2.99? 3.0 : y;",
"	return setNewColor(color, y);",
"}",

//----------------------------------------------------------
"void main() {",
	
"	if(vShape < 0.98 ){",
"		vec3 color=applyMyFilter();",
//"		color=mix(color, vec3(200.0/255.0, 210.0/255.0, 225.0/255.0), 0.1);",
"		gl_FragColor=vec4(color.xyz, 1.0);",
"	}",
"	else{",
"		gl_FragColor=texture2D(uBasis, vUv);",
"	}",
"}"
	].join( "\n" )

};

