/*
Simple Shader

*/

SimpleShader = {

	uniforms: {
    "ucolor_top": {
      "name": "ucolor_top",
      "type": "v4",
      "glslType": "vec4",
      "description": ""
    },
    "ucolor_topleft": {
      "name": "ucolor_topleft",
      "type": "v4",
      "glslType": "vec4",
      "description": ""
    },
    "ucolor_topright": {
      "name": "ucolor_topright",
      "type": "v4",
      "glslType": "vec4",
      "description": ""
    },
    "ucolor_bottom": {
      "name": "ucolor_bottom",
      "type": "v4",
      "glslType": "vec4",
      "description": ""
    },
    "ucolor_bottomleft": {
      "name": "ucolor_bottomleft",
      "type": "v4",
      "glslType": "vec4",
      "description": ""
    },
    "ucolor_bottomright": {
      "name": "ucolor_bottomright",
      "type": "v4",
      "glslType": "vec4",
      "description": ""
    },
	},

	vertexShader: [

"uniform mat4 modelViewMatrix;",
"uniform mat4 projectionMatrix;",

"attribute vec4 a_position;",
"attribute vec4 a_color;",
"attribute vec2 a_textcoord;",


"varying vec4 vColor;",
"varying vec2 vUv;",

"void main() {",

"	 vColor=a_color;",
"	 vUv = a_textcoord;",

    // This sets the position of the vertex in 3d space. The correct math is
    // provided below to take into account camera and object data.
"",
"    vec4 mv_position = modelViewMatrix * a_position;",
 
"    gl_Position = projectionMatrix * mv_position;",
//"		gl_Position=vec4(a_position.xy, 0.0, 1.0);",

"}"

	].join( "\n" ),

	fragmentShader: [
"precision highp float;",

"uniform vec4 ucolor_top;",
"uniform vec4 ucolor_topleft;",
"uniform vec4 ucolor_topright;",
"uniform vec4 ucolor_bottom;",
"uniform vec4 ucolor_bottomleft;",
"uniform vec4 ucolor_bottomright;",

"varying vec2 vUv;",
"varying vec4 vColor;",

"float hash(float n)",
"{",
"    return fract(sin(n)*10753.5453123);",
"}",

"float noise(vec4 point)",
"{",
"    vec3 p = floor(point.xyz);",
"    vec3 f = fract(point.xyz);",
"    f = f*f*(3.0-2.0*f);",
	 	
"    float n = p.x + p.y*57.0 + 113.0*p.z;",
"    return mix(mix(mix(hash(n+  0.0), hash(n+  1.0),f.x),",
"                   mix(hash(n+157.0), hash(n+158.0),f.x),f.y),",
"               mix(mix(hash(n+113.0), hash(n+114.0),f.x),",
"                   mix(hash(n+270.0), hash(n+271.0),f.x),f.y),f.z);",
"}",


"void main() {",

    //get Angle for textureCoordinate
"	float angleDeg = atan(vUv.y - 0.5,vUv.x - 0.5) * 180.0 / 3.147;",
"	if (angleDeg < 0.0){",
"		angleDeg = angleDeg+360.0 ;",
"	}",

    //Generate Gradient
"   float lerpValue = angleDeg/360.0;",

    // the constant thickness of the antialias gradient
    // along the seam
"    float limit = .02;",

"    float lerpLimit = 1.0 - limit / (6.283185*length(vUv.xy - 0.5));",

    // avoid negative limit at the center 
"    lerpLimit = max( lerpLimit, 0.0 );",

"    float invDiff = lerpLimit/(1.0-lerpLimit);",

"    if(lerpValue>lerpLimit){",
"      lerpValue = invDiff - lerpValue*invDiff;",
"            }",

"    vec4 colour_bottom = mix(vColor,ucolor_bottom,lerpValue);",
"    vec4 colour_bottomleft = mix(vColor,ucolor_bottomleft,lerpValue);",
"    vec4 colour_bottomright = mix(vColor,ucolor_bottomright,lerpValue);",
"    vec4 colour_top = mix(vColor,ucolor_top,lerpValue);",
"    vec4 colour_topleft = mix(vColor,ucolor_topleft,lerpValue);",
"    vec4 colour_topright = mix(vColor,ucolor_topright,lerpValue);",
"	 vec4 colour_avg=(colour_bottom+colour_bottomleft+colour_bottomright+colour_top+colour_topleft+colour_topright)/6.0;",
"	 float noisef=noise(vColor);",
"	 colour_avg.xyz+= mix(-0.05, 0.05, noisef); ",

"	gl_FragColor=colour_avg;",
//"	gl_FragColor = vec4(1., 0., 0., 1.);",
"}"
	].join( "\n" )

};
