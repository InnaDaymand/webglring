/*
Inna Daymand, 2018, GaazIT

FinalReflexShader

input texture: postprocessing.rtTextureColors.texture
			   postprocessing.rtReflexFloorToObject.texture
			   
make union from two texture, as result we are getting texture with shadow, reflex to object, object

*/

FinalReflexShader = {

	uniforms: {
    "resolution": {
      "name": "resolution",
      "type": "v2",
      "glslType": "vec2",
      "description": ""
    },
    "lightPosition": {
      "name": "lightPosition",
      "type": "v3",
      "glslType": "vec3",
      "description": ""
    },
	"uFloorToObject" : {
		"name" :"uFloorToObject",
		"value" : null
	},
	"uBasis" : {
		"name" :"uBasis",
		"value" : null
	},
	},

	vertexShader: [
"precision highp float;",
"precision highp int;",

"uniform mat4 modelMatrix;", //= object.MatrixWorld
"uniform mat4 modelViewMatrix;", // = camera.matrixWorldInverse*object.matrixWorld
"uniform mat4 projectionMatrix;", // = camera.projectionMatrix
"uniform mat4 viewMatrix;", // = camera.matrixWorldInverse
"uniform mat3 normalMatrix;", // = inverse transpose of modelViewMatrix

"attribute vec3 position;",
"attribute vec3 normal;",
"attribute float shape;",

"uniform vec3 lightPosition;",
"uniform vec2 resolution;",


"varying vec3 vWorldPosition;",
"varying vec3 vWorldNormal;",
"varying vec2 vUv;",
"varying float vShape;",

"void main() {",

    // This sets the position of the vertex in 3d space. The correct math is
    // provided below to take into account camera and object data.
"	vec4 mv_position=modelViewMatrix * vec4( position, 1.0 );",
"	vec4 worldPosition = modelMatrix * vec4( position, 1.0 );",
"	vWorldPosition =worldPosition.xyz; ",

"	vWorldNormal =  mat3( modelMatrix[0].xyz, modelMatrix[1].xyz, modelMatrix[2].xyz ) * normal ;",
"   gl_Position = projectionMatrix * mv_position;",
// 	getting UV coordinates of position in input texture
"	float x1 = ((gl_Position.x/gl_Position.w)+1.0)*resolution.x/2.0;",
"	float y1 = ((gl_Position.y/gl_Position.w)+1.0)*resolution.y/2.0;",
"	vUv.x = (x1)/resolution.x;",
"	vUv.y = (y1)/resolution.y;",
//--------------------------------------------------------
"	vShape=shape;",
"}"

	].join( "\n" ),

	fragmentShader: [
	"precision highp float;",

// The texture result of rendering step 2.

	"uniform sampler2D uBasis;",
	"uniform sampler2D uFloorToObject;",
	"uniform vec2 resolution;",
	"uniform vec3 lightPosition;",
	"uniform vec3 cameraPosition;", // = camera position in world space

	"varying vec2 vUv;",
	"varying vec3 vWorldPosition;",
	"varying vec3 vWorldNormal;",
	"varying float vShape;",
	

"float normColor(vec3 c){",
"	return(c.r+c.g+c.b);",
"}",
"",
"",
"",
"void main() {",

"	vec4 floor2obj=(texture2D(uFloorToObject, vUv));",
"	vec4 basis=(texture2D(uBasis, vUv));",

"	vec3 pos= lightPosition - vWorldPosition;",
"	vec3 pos1= vec3(0.0, lightPosition.y*0.1, -lightPosition.z) - vWorldPosition;",
"	vec3 pos2= cameraPosition - vWorldPosition;",
"	vec4 color_result=vec4(0.0);",
"	if(vShape >=0.99){",
"		float ncBasis=normColor(basis.rgb);",
"		float ncFloor2Obj=normColor(floor2obj.rgb);",
"		if(ncBasis >=2.95)",
"			gl_FragColor=basis;",
"		else{",
"			if(ncBasis < ncFloor2Obj && ncFloor2Obj < 2.95){",
"				vec3 avg=(basis.rgb+floor2obj.rgb)*0.5;",
"				float dotv=max(dot(normalize(pos), normalize(vWorldNormal)), 0.0001);",
"				float dotv1=max(dot(normalize(pos1+pos), normalize(vWorldNormal)), 0.0001);",
"				float dotv2=max(dot(normalize(pos2+pos1), normalize(vWorldNormal)), 0.0001);",
"				avg=avg+((avg*dotv)+ (avg*dotv1)+ (avg*dotv2))*0.4;",
"				color_result=vec4(avg, 1.0);",
"			}",
"			else",
"				color_result=vec4(mix(basis.rgb, floor2obj.rgb, 0.3), 1.0);",
"			gl_FragColor=color_result;",
"		}",
"	}",
"	if(vShape == 0.0)",
"		gl_FragColor = vec4(mix(basis.rgb, floor2obj.rgb, 0.35), 1.0);",
"}"
	].join( "\n" )

};
