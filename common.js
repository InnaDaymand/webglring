"float normColor(vec3 color){",
"	return (color.r+color.g+color.b);",
"}",
//----------------------------------------------------------
//color balance
"	vec3 colorBalance(vec3 color){",
"		if((color.r+color.g+color.b) >=2.98)",
"			return color;",
"		float drg=color.r-color.g;",
"		if(drg < 10.0/255.0){",
"			color.g=color.g-rand(vec2(9.0, 13.0))/255.0;",
"		}",
"		if(drg >30.0/255.0){",
"			drg=drg-25.0/255.0;",
"			color.r=color.r-drg;",
"		}",
"		float dgb=color.g-color.b;",
"		if(dgb >30.0/255.0 && dgb < 50.0/255.0){",
"			dgb=dgb-rand(vec2(25.0, 30.0))/255.0;",
"			color.b=color.b +dgb;",
"		}",
"		if(dgb >=50.0/255.0 && dgb < 100.0/255.0){",
"			color.b=color.b +dgb*0.8;",
"		}",
"		if(dgb >= 100.0/255.0){",
"			color.b=color.g -  rand(vec2(15.0, 25.0))/255.0;",
"		}",
"		if(dgb < 10.0/255.0 && dgb >= 0.0){",
"			color.b=color.b - rand(vec2(9.0, 15.0))/255.0;",
"		}",
"		if(dgb < 0.0){",
"			color.b=color.b - abs(dgb)*2.0;",
"		}",
"		return color;",
"	}",
