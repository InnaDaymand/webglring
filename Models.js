var Models ={
	
	normColor : function (color){
		return (color.r+color.g+color.b);
	},
	
	lerp : function (color_src, color, alpha){
		color_src.r += ( color.r - color_src.r ) * alpha;
		color_src.g += ( color.g - color_src.g ) * alpha;
		color_src.b += ( color.b - color_src.b ) * alpha;

	},

	distance : function ( v1, v2 ) {

		var dx = v1.x - v2.x, dy = v1.y - v2.y, dz = v1.z - v2.z;

		return Math.sqrt(dx * dx + dy * dy + dz * dz);
	},

	dot : function ( v31, v32) {

		return (v31.x) * (v32.x) + (v31.y) * (v32.y) + (v31.z) * (v32.z);

	},
	
	length : function (v3) {
		return Math.sqrt(v3.x*v3.x + v3.y*v3.y + v3.z*v3.z);
	},
	
	normalize : function(v3){
		var norm3=Models.length(v3);
		var v3n=new THREE.Vector3(v3.x/norm3, v3.y/norm3, v3.z/norm3);
		return v3n;
	},
	
	cross :function (v31, v32){
		var v31n=Models.normalize(v31);
		var v32n=Models.normalize(v32);
		var ax = v31n.x, ay = v31n.y, az = v31n.z;
		var bx = v32n.x, by = v32n.y, bz = v32n.z;

		
		var resultc= new THREE.Vector3(
		ay * bz - az * by,
		az * bx - ax * bz,
		ax * by - ay * bx
		);

		return resultc;
	},

	dotNormals : function (normal1, normal2){
		var v31=Models.normalize(normal1);
		var v32=Models.normalize(normal2);
		return (v31.x) * (v32.x) + (v31.y) * (v32.y) + (v31.z) * (v32.z);
	},
	
	setNewColor :function (infcolor, norm){
		if(norm == 3){
			infcolor.r =1;
			infcolor.g =1;
			infcolor.b =1;
			return;
		}
		if((infcolor.r+infcolor.g+infcolor.b) == 0)
			return;
		var newRED=(infcolor.r*norm)/(infcolor.r+infcolor.g+infcolor.b);
		var newGREEN=(infcolor.g*norm)/(infcolor.r+infcolor.g+infcolor.b);
		var newBLUE=(infcolor.b*norm)/(infcolor.r+infcolor.g+infcolor.b);
		newRED=newRED>1?1:newRED;
		newGREEN=newGREEN>1?1:newGREEN;
		newBLUE=newBLUE>1?1:newBLUE;
		infcolor.r=newRED;
		infcolor.g=newGREEN;
		infcolor.b=newBLUE;
	},

	normFactorCross : function (value){
		return (((value -0.2)*2)/(1-0.2)) -1; // 0.2 ---- 1.0
	},
	
	normFactorColor : function (value){
		return ((2*value - 3.5)/2.5); 	// 0.5 ---- 3.0
	},
	
	normFactorDistance : function (value){
		return (((value - 0.01)*2)/(0.3 - 0.01)) - 1; // 0.01 ---- 0.3
	},

	normFactorDot : function (value){
		return (((value +0.7)*2)/1.65) -1;
	},
	
	
};
