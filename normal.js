/*
normalShader

write to variable gl_FragColor normal vector, and sign of object

*/

normalShader = {

	uniforms: {
    "resolution": {
      "name": "resolution",
      "type": "v2",
      "glslType": "vec2",
      "description": ""
    },
 	"uBasis" : {
		"name" :"uBasis",
		"value" : null
	},
	},

	vertexShader: [
"precision highp float;",
"precision highp int;",

"uniform mat4 modelMatrix;", //= object.MatrixWorld
"uniform mat4 modelViewMatrix;", // = camera.matrixWorldInverse*object.matrixWorld
"uniform mat4 projectionMatrix;", // = camera.projectionMatrix
"uniform mat4 viewMatrix;", // = camera.matrixWorldInverse
"uniform mat3 normalMatrix;", // = inverse transpose of modelViewMatrix

"attribute vec3 position;",
"attribute vec3 normal;",
"attribute float shape;",
"attribute float isContour;",
"attribute float isBorder;",
"attribute float randomValue;",

"uniform vec3 lightPosition;",
"uniform vec2 resolution;",


"varying vec3 vWorldPosition;",
"varying vec3 vWorldNormal;",
"varying vec2 vUv;",
"varying float vShape;",
"varying float vIsContour;",
"varying float vIsBorder;",
"varying float vRandomValue;",

"void main() {",

    // This sets the position of the vertex in 3d space. The correct math is
    // provided below to take into account camera and object data.
"	vec4 mv_position=modelViewMatrix * vec4( position, 1.0 );",
"	vec4 worldPosition = modelMatrix * vec4( position, 1.0 );",
"	vWorldPosition =worldPosition.xyz; ",

"	vWorldNormal =  mat3( modelMatrix[0].xyz, modelMatrix[1].xyz, modelMatrix[2].xyz ) * normal ;",
"   gl_Position = projectionMatrix * mv_position;",
"	float x1 = ((gl_Position.x/gl_Position.w)+1.0)*resolution.x/2.0;",
"	float y1 = ((gl_Position.y/gl_Position.w)+1.0)*resolution.y/2.0;",
"	vUv.x = (x1)/resolution.x;",
"	vUv.y = (y1)/resolution.y;",
"	vShape=shape;",
"	vIsContour=isContour;",
"	vIsBorder=isBorder;",
"	vRandomValue=randomValue;",

"}"

	].join( "\n" ),

	fragmentShader: [
	"precision highp float;",


	"uniform sampler2D uBasis;",
	
	"varying vec2 vUv;",
	"varying vec3 vWorldPosition;",
	"varying vec3 vWorldNormal;",
	"varying float vShape;",
	"varying float vIsContour;",
	"varying float vIsBorder;",
	"varying float vRandomValue;",
	

"void main() {",
"	gl_FragColor=vec4(normalize(vWorldNormal), vShape);",
"}"
	].join( "\n" )

};
