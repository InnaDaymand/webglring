/*
contrastShader

use as input reflex textures that is result of rendering of ReflexShader

*/

causticsShader = {

	uniforms: {
    "resolution": {
      "name": "resolution",
      "type": "v2",
      "glslType": "vec2",
      "description": ""
    },
    "lightPosition": {
      "name": "lightPosition",
      "type": "v3",
      "glslType": "vec3",
      "description": ""
    },
	"uBasis" : {
		"name" :"uBasis",
		"value" : null
	},
	"uNormalTexture":{
		"name" :"uNormalTexture",
		"value" : null
	},
	"reflexFloor" : {
		"name" : "reflexFloor",
	},
	"reflexGold" : {
		"name" : "reflexGold",
	},
	"emissionGold" : {
		"name" : "emissionGold",
	},
	"emissionFloor" : {
		"name" : "emissionFloor",
	},
	"countReflex" : {
		"name" : "countReflex",
	},
	},

	vertexShader: [
"precision highp float;",
"precision highp int;",

"uniform mat4 modelMatrix;", //= object.MatrixWorld
"uniform mat4 modelViewMatrix;", // = camera.matrixWorldInverse*object.matrixWorld
"uniform mat4 projectionMatrix;", // = camera.projectionMatrix
"uniform mat4 viewMatrix;", // = camera.matrixWorldInverse
"uniform mat3 normalMatrix;", // = inverse transpose of modelViewMatrix

"attribute vec3 position;",
"attribute vec3 normal;",
"attribute float shape;",
"attribute float isContour;",
"attribute float isBorder;",
"attribute float randomValue;",

"uniform vec3 lightPosition;",
"uniform vec2 resolution;",


"varying vec3 vWorldPosition;",
"varying vec3 vWorldNormal;",
"varying vec2 vUv;",
"varying float vShape;",
"varying float vIsContour;",
"varying float vIsBorder;",
"varying float vRandomValue;",

"void main() {",

    // This sets the position of the vertex in 3d space. The correct math is
    // provided below to take into account camera and object data.
"	vec4 mv_position=modelViewMatrix * vec4( position, 1.0 );",
"	vec4 worldPosition = modelMatrix * vec4( position, 1.0 );",
"	vWorldPosition =worldPosition.xyz; ",

"	vWorldNormal =  mat3( modelMatrix[0].xyz, modelMatrix[1].xyz, modelMatrix[2].xyz ) * normal ;",
"   gl_Position = projectionMatrix * mv_position;",
"	float x1 = ((gl_Position.x/gl_Position.w)+1.0)*resolution.x/2.0;",
"	float y1 = ((gl_Position.y/gl_Position.w)+1.0)*resolution.y/2.0;",
"	vUv.x = (x1)/resolution.x;",
"	vUv.y = (y1)/resolution.y;",
"	vShape=shape;",
"	vIsContour=isContour;",
"	vIsBorder=isBorder;",
"	vRandomValue=randomValue;",

"}"

	].join( "\n" ),

	fragmentShader: [
	"precision highp float;",

// The texture result of rendering step 2.
	"uniform mat4 projectionMatrix;", // = camera.projectionMatrix

	"uniform sampler2D uBasis;",
	"uniform sampler2D uNormalTexture;",
	"uniform float lightPosition;",
	
	"varying vec2 vUv;",
	"varying vec3 vWorldPosition;",
	"varying vec3 vWorldNormal;",
	"varying float vShape;",
	"varying float vIsContour;",
	"varying float vIsBorder;",
	"varying float vRandomValue;",
	
"float normColor(vec3 color){",
"	return (color.r+color.g+color.b);",
"}",
//"	vec3 colorRedBalance(vec3 color){",
//"		color.b=color.b - redBalance;",
//"		return color;",
//"",
//"	}",

"float rand(vec2 v){",
"	return (vRandomValue*(v.y - v.x) + v.x);",
"",
"",
"}",
//color balance
"	vec3 colorBalance(vec3 color){",
"		if(color.g < color.b)",
"			color.b=color.g;",
"		float maxColorNorm=2.98;",
"		float kNorm=1.0/255.0;",
"		float min_drg=10.0;",
"		float max_drg=30.0;",
"		if(normColor(color) >=maxColorNorm)",
"			return color;",
"		float drg=color.r-color.g;",
"		if(drg < min_drg*kNorm){",
"			color.g=color.g-rand(vec2(min_drg, min_drg+5.0))*kNorm;",
"		}",
"		if(drg >max_drg*kNorm){",
"			drg=drg-(max_drg - 5.0)*kNorm;",
"			color.r=color.r-drg;",
"		}",
"		float min_dgb=30.0;",
"		float max_dgb=50.0;",
"		float limit_dgb=100.0;",
"		float dgb=color.g-color.b;",
"		if(dgb >min_dgb*kNorm && dgb < max_dgb*kNorm){",
"			dgb=dgb-rand(vec2(min_dgb - 5.0, min_dgb))*kNorm;",
"			color.b=color.b +dgb;",
"		}",
"		if(dgb >=max_dgb*kNorm && dgb < limit_dgb*kNorm){",
"			color.b=color.b +dgb*0.8;",
"		}",
"		if(dgb >= limit_dgb*kNorm){",
"			color.b=color.g -  rand(vec2(min_drg, min_dgb))*kNorm;",
"		}",
"		if(dgb < min_drg*kNorm && dgb > 0.0){",
"			color.b=color.b - rand(vec2(min_drg, min_drg+5.0))*kNorm;",
"		}",
"		if(dgb <= 0.0){",
"			color.b=color.g - rand(vec2(min_dgb/2.0, min_dgb/2.0+5.0))*kNorm;",
"		}",
//"		color=colorRedBalance(color);",
"		return color;",
"	}",

"vec2 getUVCoordinates(vec3 inCoordinates){",
"   vec4 pos = projectionMatrix * inCoordinates;",
"	vec2 res=vec2(0.0);",
"	float x1 = ((gl_Position.x/gl_Position.w)+1.0)*resolution.x/2.0;",
"	float y1 = ((gl_Position.y/gl_Position.w)+1.0)*resolution.y/2.0;",
"	res.x = (x1)/resolution.x;",
"	res.y = (y1)/resolution.y;",
"	return res;",
"}",

"void main() {",
"	vec3 I=vWorldPosition - lightPosition;",
"	vec3 reflex=reflect( I, normalize(vWorldNormal));",
"	vec2 uvReflex=getUVCoordinates(reflex);",
"	while(1 > 0){",
"		vec4 basis=(texture2D(uBasis, vUv));",
"		vec4 data=texture2D(uNormalTexture, vUv);",
"		vec3 normal=data.xyz;",
"		float shape=data.a;",
"	}",
"	float norm=normColor(basis.rgb);",
"	if(norm < 2.95 && vShape > 0.98 && vIsContour <0.98 && vIsBorder < 0.98){",
//"		float luminace= basis.r * 0.299 + basis.g * 0.587 + basis.b * 0.114;",
"		vec4 color=basis;",
"    	color=brightnessAdjust(color, brightness); ",
"    	color=contrastAdjust(color, contrast); ",
"		gl_FragColor=vec4(color.rgb, 1.0);",
"	}",
"	else",
"	if(norm < 2.95 && vShape > 0.98 && (vIsContour >0.98 )){",
"	    gl_FragColor = saturationMatrix(saturation) * basis; ",
"    	gl_FragColor = vibrance(gl_FragColor, _vibrance);",
"    	gl_FragColor = shiftHue(gl_FragColor.rgb, _hue);",
"    	gl_FragColor.a = 1.0;",
"	}",
"	if(norm >=2.95 || vShape < 0.98 || vIsBorder > 0.98)",
"		gl_FragColor=basis;",
"}"
	].join( "\n" )

};
