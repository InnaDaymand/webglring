/*
Inna Daymand, 2018, GaazIT

diamondMaterialShader (not ended)

use as input cube texture that is result of rendering of cubeCamera

*/

DiamondMaterialShader = {

	uniforms: {
    "resolution": {
      "name": "resolution",
      "type": "v2",
      "glslType": "vec2",
      "description": ""
    },
    "lightPosition": {
      "name": "lightPosition",
      "type": "v3",
      "glslType": "vec3",
      "description": ""
    },
	"u_texture" : {
		"name" :"u_texture",
		"value" : null
	},
	"ior": { 
		"name" : "ior",
		"type": "v3",
		"glslType" : "vec3",
		"value": "", 
	},
	"fresnelBias": { 
		"value": "0.01" 
	},
	"fresnelPower": { 
		"value": "2.0" 
	},
	"fresnelScale": { 
		"value": ".5" 
	},
	},

	vertexShader: [
"precision highp float;",
"precision highp int;",

"uniform mat4 modelMatrix;", //= object.MatrixWorld
"uniform mat4 modelViewMatrix;", // = camera.matrixWorldInverse*object.matrixWorld
"uniform mat4 projectionMatrix;", // = camera.projectionMatrix
"uniform mat4 viewMatrix;", // = camera.matrixWorldInverse
"uniform mat3 normalMatrix;", // = inverse transpose of modelViewMatrix
"uniform vec3 cameraPosition;", // = camera position in world space

"attribute vec3 position;",
"attribute vec3 normal;",
"attribute vec2 uv;",

"uniform vec2 resolution;",
"uniform vec3 lightPosition;",
"uniform vec3 ior;",
"uniform float fresnelBias;",
"uniform float fresnelPower;",
"uniform float fresnelScale;",

"varying vec3 vReflect;",
"varying vec3 vRefract[6];",
"varying float vReflectionFactor;",
"varying vec3 vWorldPosition;",

"void main() {",

"	 vec3 lNormal=normalize(mat3(normalMatrix)*normal);",
    // This sets the position of the vertex in 3d space. The correct math is
    // provided below to take into account camera and object data.
"	 vec4 mv_position=modelViewMatrix * vec4( position, 1.0 );",
"	vec4 worldPosition = modelMatrix * vec4( position, 1.0 );",
"	vWorldPosition =worldPosition.xyz; ",

"	vec3 worldNormal = normalize( mat3( modelMatrix[0].xyz, modelMatrix[1].xyz, modelMatrix[2].xyz ) * normal );",

"	vec3 I = worldPosition.xyz - cameraPosition;",
"	vec3 L = worldPosition.xyz - lightPosition;",
"	vec3 D = normalize(I);",
"	vec3 iorN=0.5*ior;",
"	vReflect = reflect( D, worldNormal );",
"	vRefract[0] = refract( normalize( D ), worldNormal, iorN.r );",
"	vRefract[1] = refract( normalize( D ), worldNormal, iorN.g );",
"	vRefract[2] = refract( normalize( D ), worldNormal, iorN.b );",
"	D=normalize(L);",
"	vRefract[3] = refract( normalize( D ), worldNormal, iorN.r );",
"	vRefract[4] = refract( normalize( D ), worldNormal, iorN.g );",
"	vRefract[5] = refract( normalize( D ), worldNormal, iorN.b );",
"	vReflectionFactor = fresnelBias + fresnelScale * pow( 1.0 + dot( normalize( D ), worldNormal ), fresnelPower );",
"   gl_Position = projectionMatrix * mv_position;",

"}"

	].join( "\n" ),

	fragmentShader: [
	"precision highp float;",

	"uniform sampler2D u_texture;",
	"uniform vec2 resolution;",

	"varying vec3 vReflect;",
	"varying vec3 vRefract[6];",
	"varying float vReflectionFactor;",
	"varying vec3 vWorldPosition;",
	
// constants	
	"#define pi 3.14159275358979", 
	"#define EPS 4e-4",

"void main() {",
"	vec2 vUv=vec2(0.0);",
"	float x1 = ((vReflect.x/vReflect.z)+1.0)*resolution.x/2.0;",
"	float y1 = ((vReflect.y/vReflect.z)+1.0)*resolution.y/2.0;",
"	vUv.x = (x1)/resolution.x;",
"	vUv.y = (y1)/resolution.y;",
"	vec4 reflectedColor= texture2D(u_texture, vUv);",

"	vec4 refractedColorC = vec4( 1.0 );",
"	vec4 refractedColorL=vec4(1.0);", 

"	x1 = ((vRefract[0].x/vRefract[0].z)+1.0)*resolution.x/2.0;",
"	y1 = ((vRefract[0].y/vRefract[0].z)+1.0)*resolution.y/2.0;",
"	vUv.x = (x1)/resolution.x;",
"	vUv.y = (y1)/resolution.y;",
"	refractedColorC.r = texture2D(u_texture, vUv).r;",

"	x1 = ((vRefract[1].x/vRefract[1].z)+1.0)*resolution.x/2.0;",
"	y1 = ((vRefract[1].y/vRefract[1].z)+1.0)*resolution.y/2.0;",
"	vUv.x = (x1)/resolution.x;",
"	vUv.y = (y1)/resolution.y;",
"	refractedColorC.g = texture2D(u_texture, vUv).g;",

"	x1 = ((vRefract[2].x/vRefract[2].z)+1.0)*resolution.x/2.0;",
"	y1 = ((vRefract[2].y/vRefract[2].z)+1.0)*resolution.y/2.0;",
"	vUv.x = (x1)/resolution.x;",
"	vUv.y = (y1)/resolution.y;",
"	refractedColorC.b = texture2D(u_texture, vUv).b;",

"	x1 = ((vRefract[3].x/vRefract[3].z)+1.0)*resolution.x/2.0;",
"	y1 = ((vRefract[3].y/vRefract[3].z)+1.0)*resolution.y/2.0;",
"	vUv.x = (x1)/resolution.x;",
"	vUv.y = (y1)/resolution.y;",
"	refractedColorL.r = texture2D(u_texture, vUv).r;",

"	x1 = ((vRefract[4].x/vRefract[4].z)+1.0)*resolution.x/2.0;",
"	y1 = ((vRefract[4].y/vRefract[4].z)+1.0)*resolution.y/2.0;",
"	vUv.x = (x1)/resolution.x;",
"	vUv.y = (y1)/resolution.y;",
"	refractedColorL.g = texture2D(u_texture, vUv).g;",

"	x1 = ((vRefract[5].x/vRefract[5].z)+1.0)*resolution.x/2.0;",
"	y1 = ((vRefract[5].y/vRefract[5].z)+1.0)*resolution.y/2.0;",
"	vUv.x = (x1)/resolution.x;",
"	vUv.y = (y1)/resolution.y;",
"	refractedColorL.b = texture2D(u_texture, vUv).b;",
//"	gl_FragColor=vec4(1.0-clamp( vReflectionFactor, 0.0, 1.0 ));",
"	gl_FragColor = mix( (refractedColorL+refractedColorC)*0.5, reflectedColor, 0.75*(clamp( vReflectionFactor, 0.0, 1.0 )) );",

"}"
	].join( "\n" )

};
