var worker;
var blockY = 1;
var blockX = 1;
var startX, startY, completed = 0;
var startXA, startYA;

var scene, camera, texture, renderer, loader, sceneId, SCREEN_WIDTH, SCREEN_HEIGHT;
var cache;

importScripts( 'threejs/build/three.js' );
importScripts( './InfoRendering.js' );
importScripts( './Models.js' );

var iRenderBlock;

self.onmessage = function( e ) {

	var data = e.data;
	if ( ! data ) return;

	if ( data.init ) {

		worker = data.worker;
		blockY = data.blockSizeY;
		blockX = data.blockSizeX;

		if ( ! renderer ) renderer = new THREE.WEBGLRaytracingRendererWorker();
		if ( ! loader ) loader = new THREE.ObjectLoader();
		
		completed = 0;

	}

	if ( data.scene ) {

		scene = loader.parse( data.scene );
		camera = loader.parse( data.camera );
		cache = data.cache;
		sceneId = data.sceneId;
		renderer.initRenderAddCanvas(scene, camera);
		texture=data.texture;
		
	}

	if ( data.render && scene && camera ) {

		startX = data.sX;
		startY = data.sY;
		iRenderBlock=data.block;
		renderer.render( scene, camera );
	}

	if ( data.resize && renderer ) {
		startYA=data.startYA,
		startXA=data.startXA,
		SCREEN_HEIGHT=data.height;
		SCREEN_WIDTH=data.width;
	}
}

THREE.WEBGLRaytracingRendererWorker = function ( parameters ) {

	console.log( 'THREE.RaytracingRendererWorker', THREE.REVISION );

	parameters = parameters || {};

	var scope = this;
	var objects=[];
	var lights=[];
	var modelViewMatrix = new THREE.Matrix4();
	var cameraNormalMatrix = new THREE.Matrix3();
	var transposeInverseMatrix=new THREE.Matrix4();
	var maxRecursionDepth = 2	;
	var origin = new THREE.Vector3();
	var direction = new THREE.Vector3();
	var cameraPosition=new THREE.Vector3();
	var raycaster = new THREE.Raycaster( origin, direction );
	var raycasterLight = new THREE.Raycaster();
	var pixelColor=new THREE.Color();
	var data = {};
	var textureColor=new THREE.Color();
	var tmpColor=new THREE.Color();
	var infor, inforIntersection;
	var tmpArrayRendering=new Array();
	var apertureX=10;
	var apertureY=10;
	

	var renderBlock = ( function () {

		var blockSizeY = blockY;
		var blockSizeX = blockX;


		var pixelColor = new THREE.Color();
		var dominance =-2;	
		return function renderBlock() {
			var H=camera.near*Math.tan(THREE.Math.degToRad( camera.fov * 0.5 ));
			var W=H*camera.aspect;
			
			for(var r =0; r < blockSizeY; r++){
				iRenderBlock.arrayInfoRendering.push(new Array());
			}
			
			for ( var y = startY; y < (startY + blockSizeY); y +=apertureY ) {

				for ( var x = startX; x < (startX + blockSizeX); x +=apertureX) {

					// select randomize point in aperture 3x3
					
					var xpoint, ypoint, apXEnd;
					dominance=-2;
					infor=null;
					if((x+apertureX-1) < (startX + blockSizeX)){
						apXEnd=x+apertureX;
					}
					else{
						apXEnd=startX+blockSizeX;
					}
					dominance=neighborhoodOnBorder(x, y, apXEnd, y+apertureY-1);
					processRayTracing(0, W, H, dominance); //plane
					processRayTracing(1, W, H, dominance); //ring
					processRayTracing(2, W, H, dominance); //stone
					for(var i=0; i < tmpArrayRendering.length; i++){
						var inforTMP=tmpArrayRendering[i].infor;
						iRenderBlock.arrayInfoRendering[inforTMP.yShape - startY].push(inforTMP);
					}
					
				}

			}

			// Use transferable objects! :)
			self.postMessage( {
				result:iRenderBlock,
				blockSizeY: blockY,
				sceneId: sceneId,
				time: Date.now() - reallyThen, // time for this renderer
			});


			// OK Done!
			completed ++;

		};

	}() );
	
	function isPixelOnBorder(current_index){
		var infor=tmpArrayRendering[current_index].infor;
		for(var i=-1; i<2; i++){
			for(var j=-1; j <2; j++){
				var index1=current_index + i*(apertureX) + j;
				if(index1 < 0 || index1 >= tmpArrayRendering.length)
					continue;
				var infor1=tmpArrayRendering[index1].infor;
				if(Math.abs(infor1.xShape - infor.xShape)>1)
					continue;
				var dist=Models.distance(infor.point, infor1.point);
				if(infor.shape == infor1.shape && dist < 0.09)
					continue;
				else{
					return true;
				}
			}
		}
		return false;
	}

	
	function processRayTracing(shape, W, H, dominance){
		if(tmpArrayRendering.length <=0)
			return;
		var indexTMP, inforTMP;
		var avgNormColor=0, count=0;
		var pointp=new THREE.Vector4();
		for(var i=0; i < tmpArrayRendering.length; i++){
			inforTMP=tmpArrayRendering[i].infor;
			if(inforTMP.shape == shape){
				pointp.set(inforTMP.point.x, inforTMP.point.y, inforTMP.point.z, 1);
				pointp.applyMatrix4(camera.matrixWorldInverse);
				pointp.applyMatrix4(camera.projectionMatrix);
				var x1=Math.round(SCREEN_WIDTH/2 + ((pointp.x/pointp.w)*SCREEN_WIDTH)/2);
				var y1=Math.round(SCREEN_HEIGHT/2 - ((pointp.y/pointp.w)*SCREEN_HEIGHT)/2)-1;
				index1=((startYA - y1)) *blockX*4 +(x1- startXA)*4;
				inforTMP.textureColor.r=texture[index1+0]/255;
				inforTMP.textureColor.g=texture[index1+1]/255;
				inforTMP.textureColor.b=texture[index1+2]/255;
				inforTMP.colorShape.copy(inforTMP.textureColor);
				inforTMP.normTextureColor=Models.normColor(inforTMP.textureColor);
				avgNormColor+=inforTMP.normTextureColor;
				count++;
			}
		}
		if(count == 0)
			return;
		avgNormColor=avgNormColor/count;
		for(var i=0; i < tmpArrayRendering.length;i++){
			inforIntersection=tmpArrayRendering[i];
			if(inforIntersection.infor.shape==shape && inforIntersection.infor.normTextureColor >= (avgNormColor - 0.315) && inforIntersection.infor.normTextureColor <= (avgNormColor + 0.315))
				break;
		}
		xpoint = inforIntersection.infor.xShape;
		ypoint = inforIntersection.infor.yShape;
		
		// spawn primary ray at pixel position
		textureColor.copy(inforIntersection.infor.textureColor);
		inforIntersection.infor.calcPoint=true;
		
		origin.copy( cameraPosition);
		
		direction.set( (2*xpoint/SCREEN_WIDTH-1)*W, (2*(SCREEN_HEIGHT -1- ypoint)/SCREEN_HEIGHT -1)*H, -camera.near);
		direction.applyMatrix3( cameraNormalMatrix );
		direction.normalize();
		spawnRay( origin, direction, pixelColor, 0, textureColor, inforIntersection);
		var empty=false;
		if(pixelColor.r == 0 || pixelColor.g == 0 || pixelColor.b == 0){
			pixelColor.add(textureColor);
			empty=true;
		}
		if(count == 100 && dominance == 0 && empty == true)
			return;
		infor=inforIntersection.infor;
		infor.colorShape.copy(pixelColor);
		var normCalcColor=Models.normColor(infor.colorShape);
		for(var i=0; i < tmpArrayRendering.length; i++){
			var inforTMP=tmpArrayRendering[i].infor;
			if(inforTMP.shape == infor.shape && infor.shape > 0 && dominance >=0){
				inforTMP.onBorder=isPixelOnBorder(i);
			}
			if(infor.xShape==inforTMP.xShape && infor.yShape==inforTMP.yShape){
				setColorOfPixelInAperture(inforTMP, infor, inforTMP.onBorder, normCalcColor);
				continue;
			}
			else{
				if((infor.shape == inforTMP.shape)){
					if((infor.shape == 0 && empty ==false) || infor.shape > 0){
						setColorOfPixelInAperture(inforTMP, infor, inforTMP.onBorder, normCalcColor);
					}
					if(infor.infoLReflex.length >0){
						var xOffset=inforTMP.xShape - infor.xShape;
						var yOffset = inforTMP.yShape - infor.yShape;
						inforTMP.copyInfoLRefrex(infor, xOffset, yOffset);
					}
				}
			}	
		}
	}
	
/*
		Y1 = 1.7702 +0.0784*X1 +0.2147*X2 +0.4692*X3 +0.4884*X4 +0.0781*X5 -0.0393*X1*X2 +0.0197*X1*X3 -0.0196*X1*X5 +0.039*X2*X3 +0.1367*X2*X4 -0.1959*X3*X4 -
		0.0193*X3*X5 +0.0389*X4*X5 -0.0195*X1*X2*X3 -0.039*X1*X2*X4 -0.0195*X1*X2*X5 -
		0.0199*X1*X3*X4 +0.0777*X2*X3*X4 -0.0192*X2*X3*X5							
																										
		Y1 = 1.7604 +0.0882*X1 +0.2049*X2 +0.4593*X3 +0.4982*X4 +0.0682*X5 -0.0294*X1*X2 +0.0296*X1*X3 -0.01*X1*X4 -0.0098*X1*X5 +0.0292*X2*X3 +0.1465*X2*X4 -
		0.01*X2*X5 -0.1861*X3*X4 -0.0292*X3*X5 +0.0488*X4*X5 -0.0096*X1*X2*X3 -0.0489*X1*X2*X4 -0.0096*X1*X2*X5 -
		0.0297*X1*X3*X4 +0.0095*X1*X3*X5 +0.0875*X2*X3*X4 -0.029*X2*X3*X5							
							
*/	
	function setColorOfPixelInAperture(infor_tmp, infor, isBorder, normCalcColor){
		if(infor_tmp.normTextureColor > 2.8 )
			return;
		var x1_f=Models.distance(infor.point, infor_tmp.point);
		var x2_f=Models.dotNormals(infor.normal, infor_tmp.normal);
		var x3_f=infor_tmp.normTextureColor;
		var x4_f=normCalcColor;
		var x5_f=isBorder;
		var X1=Models.normFactorDistance(x1_f); 
		var X2=Models.normFactorDot(x2_f); 
		var X3=Models.normFactorColor(x3_f); 
		var X4=Models.normFactorColor(x4_f); 
		var X5=(x5_f == true) ?1 : -1;
		var y =1.7604 +0.0882*X1 +0.2049*X2 +0.4593*X3 +0.4982*X4 +0.0682*X5 -0.0294*X1*X2 +0.0296*X1*X3 -0.01*X1*X4 -0.0098*X1*X5 +0.0292*X2*X3 +0.1465*X2*X4 -
		0.01*X2*X5 -0.1861*X3*X4 -0.0292*X3*X5 +0.0488*X4*X5 -0.0096*X1*X2*X3 -0.0489*X1*X2*X4 -0.0096*X1*X2*X5 -
		0.0297*X1*X3*X4 +0.0095*X1*X3*X5 +0.0875*X2*X3*X4 -0.029*X2*X3*X5;
		y = y > 3 ? 3 :y;
		if(y < infor_tmp.normTextureColor)
			return;
		Models.setNewColor(infor_tmp.colorShape, y);
	}
	
	function pointObject(point){
		var H=camera.near*Math.tan(THREE.Math.degToRad( camera.fov * 0.5 ));
		var W=H*camera.aspect;
		var ret=false;
		direction.set( (2*point.infor.xShape/SCREEN_WIDTH-1)*W, (2*(SCREEN_HEIGHT -1- point.infor.yShape)/SCREEN_HEIGHT -1)*H, -camera.near);
		direction.applyMatrix3( cameraNormalMatrix );
		direction.normalize();
		raycaster.ray.origin.copy(cameraPosition);
		raycaster.ray.direction.copy(direction);
		var object=scene.getObjectByName("Ring");
		if(object == null)
			return false;
		var intersection=raycaster.intersectObject(object, true );
		if(intersection.length >0){
			ret=true;
		}
		if (ret == false){
			object=scene.getObjectByName("Plane");
			if(object == null)
				return false;
			intersection=raycaster.intersectObject(object, true );
		}	
		if(intersection.length >0){
			point.intersection.point.copy(intersection[0].point);
			point.intersection.uvCoordinates.copy(intersection[0].uv);
			point.intersection.face=intersection[0].face;
			point.intersection.object=intersection[0].object;
		}
		return ret;
	}

	
	function neighborhoodOnBorder(xSet, ySet, apXEnd, apYEnd){
		tmpArrayRendering.splice(0, tmpArrayRendering.length);
		dominance=-2;
		var countPointPlane=0, countPointObjectRing=0, countPointObjectStone=0;
		for(var y1=ySet; y1 <= apYEnd; y1++ ){
			for(var x1 =xSet; x1 < apXEnd; x1++){
				var point=new THREE.InfoRenderingIntersection();
				point.infor.xShape=x1;
				point.infor.yShape=y1;
				var ret=pointObject(point);
				if(point.intersection.object != null){
					point.infor.point.copy(point.intersection.point);
					point.infor.uvCoordinates.copy(point.intersection.uvCoordinates);
					point.infor.normal.copy(point.intersection.face.normal);
					if(ret ==true){
						if(point.intersection.object.name == "G_White_1"){
							point.infor.shape=1;
							countPointObjectRing+=1;
						}
						if(point.intersection.object.name == "D_White_1"){
							point.infor.shape=2;
							countPointObjectStone+=1;
						}
					}
					else{
						point.infor.shape=0;
						countPointPlane+=1;
					}
					tmpArrayRendering.push(point);
				}
			}
		}
		if(countPointPlane >0 && countPointObjectRing >0){
			dominance = 1;
		}
		if(countPointPlane >0 && countPointObjectStone >0){
			dominance = 2;
		}
		if(countPointPlane > countPointObjectRing && countPointPlane>countPointObjectStone){
			dominance=0;
		}
		if(countPointObjectRing > countPointPlane && countPointObjectRing > countPointObjectStone){
			dominance=1;
		}
		if(countPointObjectStone > countPointPlane && countPointObjectStone > countPointObjectRing){
			dominance=2;
		}
		return dominance;
	}
	
	this.initRenderAddCanvas = function (scene, camera){
	
		// set up object matrices and vertices for meshes

		scene.traverse( function ( object ) {

			if ( object instanceof THREE.Light && object.isAmbientLight == undefined) {

				lights.push( object );

			}
			if(object instanceof THREE.Mesh || object instanceof THREE.Group){

				objects.push(object);
			
			}
		} );
	};
	
	this.render = function ( scene, camera ) {

		reallyThen = Date.now()

		// update scene graph

		if ( scene.autoUpdate === true ) scene.updateMatrixWorld();

		// update camera matrices

		if ( camera.parent === null ) camera.updateMatrixWorld();

		camera.matrixWorldInverse.getInverse( camera.matrixWorld );
		cameraPosition.setFromMatrixPosition( camera.matrixWorld );

		//

		cameraNormalMatrix.getNormalMatrix( camera.matrixWorld );

		renderBlock();

	};

	var goldIor=new THREE.Color(0.989, 0.876, 0.399);
	var goldAmbient=new THREE.Color(0.24725, 0.1995, 0.0745);
	var goldDiffuse=new THREE.Color(0.75164, 0.60648, 0.22648);
	var goldSpecular=new THREE.Color(0.628281, 0.555802, 0.366065);
	var goldColor=new THREE.Color(247/255,204/255,143/255);
	var goldEmissive=new THREE.Color(221/255,204/255,143/255);
	var goldIntensityEmissive=0.65;
	var goldShininess=2.2;
	var planeShininess=.85;
	var planeIntensityEmissive=0.82;
	var planeAmbient=new THREE.Color(0x302e14);
	var planeSpecular=new THREE.Color(0xefeef0);
	var planeIor=new THREE.Color(0.989, 0.876, 0.799);
	
	var pointp=new THREE.Vector4();
	function readPixels(point, textureColor){
		pointp.set(point.x, point.y, point.z, 1);
		pointp.applyMatrix4(camera.matrixWorldInverse);
		pointp.applyMatrix4(camera.projectionMatrix);
		var x1=Math.round(SCREEN_WIDTH/2 + ((pointp.x/pointp.w)*SCREEN_WIDTH)/2);
		var y1=Math.round(SCREEN_HEIGHT/2 - ((pointp.y/pointp.w)*SCREEN_HEIGHT)/2)-1;
		var index=((startYA - y1)) *blockX*4 +(x1- startXA)*4;
		if(index > 0 && index < texture.length && x1 <=(startXA+blockX)){
			textureColor.r=texture[index+0]/255;
			textureColor.g=texture[index+1]/255;
			textureColor.b=texture[index+2]/255;
		}
		else{
			textureColor.copy(planeAmbient);
		}
	}
	
	var spawnRay = ( function () {
		var eyeVector = new THREE.Vector3();
		var normalVector = new THREE.Vector3();

		var localPoint = new THREE.Vector3();
		var reflectionVector = new THREE.Vector3();
		var tmpColor=new THREE.Color(0x000000);
		var tmpColors = [];

		for ( var i = 0; i < maxRecursionDepth; i ++ ) {

			tmpColors[ i ] = new THREE.Color(0x000000);

		}
		var tmpTextureColor=new THREE.Color();
		return function spawnRay( rayOrigin, rayDirection, outputColor, recursionDepth, textureColor, infoRenderIntersection ) {
			var intersections;
			if(infoRenderIntersection.intersection.object == null )
				return;
			var infoRender=infoRenderIntersection.infor;
			if(recursionDepth >0){
				var ray = raycaster.ray;

				ray.origin = rayOrigin;
				ray.direction = rayDirection;

				outputColor.setRGB(0,0,0);

				intersections = raycaster.intersectObjects( objects, true );

				// ray didn't find anything
				// (here should come setting of background color?)

				if ( intersections.length === 0 ) {
					return;
				}
			}

			// ray hit
			var intersection = (recursionDepth == 0)?infoRenderIntersection.intersection : intersections[ 0 ];
			var point = intersection.point;
			
			var object = intersection.object;
			var material = object.material;
			var face = intersection.face;
			var _object = cache[ object.uuid ];
			var vertices;
			
			if(object.geometry instanceof THREE.BufferGeometry){
				vertices=_object.vertices;
				if(face.vertexNormals.length === 0){
					face.vertexNormals.push(_object.verticesNormals[face.a]);
					face.vertexNormals.push(_object.verticesNormals[face.b]);
					face.vertexNormals.push(_object.verticesNormals[face.c]);
				}
			}
			else{
				vertices= object.geometry.vertices;
			}
			
			localPoint.copy( point ).applyMatrix4( _object.inverseMatrix );
			eyeVector.subVectors(raycaster.ray.origin, point).normalize();
//THREE.SmoothShading FlatShading
			computePixelNormal( normalVector, localPoint, THREE.SmoothShading, face, vertices );
			normalVector.applyMatrix3( _object.normalMatrix ).normalize();
			
			
			if(recursionDepth > 0){
				readPixels(point, tmpTextureColor);
			}
			else{
				tmpTextureColor.copy(textureColor);
				infoRender.textureColor.copy(textureColor);
			}
			// emissive and ambient light
			if(recursionDepth == 0){
				makeLightReflex(point, normalVector, object, infoRender);
				outputColor.copy(tmpTextureColor);
				if(material instanceof THREE.RawShaderMaterial){
					outputColor.multiplyScalar(goldIntensityEmissive);
					tmpColor.copy(goldAmbient);
				}
				if(material instanceof THREE.MeshPhongMaterial){
					outputColor.multiplyScalar(planeIntensityEmissive);
					tmpColor.copy(planeAmbient);
				}
				outputColor.add(tmpColor);
			}
			if(recursionDepth >0)
				calcColorAllLight(eyeVector, normalVector, object, tmpTextureColor, outputColor, point);
// reflection
			if ( recursionDepth < maxRecursionDepth ) {
				reflectionVector.copy( rayDirection );
				reflectionVector.reflect( normalVector );

				var zColor = tmpColors[ recursionDepth ];
				
				origin.copy(reflectionVector);
				origin.multiplyScalar(0.0001);
				origin.add(point);
				spawnRay(origin, reflectionVector, zColor, recursionDepth+1, tmpTextureColor, infoRenderIntersection );
				
				if(zColor.r !=0 || zColor.g != 0 || zColor.b != 0){
					outputColor.add( zColor );
				}
				else
					if(recursionDepth == 0){
						outputColor.setRGB(0,0,0);
					}

			}

		};

	}() );

	var makeLightReflex = ( function () {
		var directionLight=new THREE.Vector3();
		var normalVectorL=new THREE.Vector3();
		var localPointL=new THREE.Vector3();
		var rayLight= raycasterLight.ray;
		var reflectionLightVector=new THREE.Vector3();
		var tmpVector=new THREE.Vector3();
		var eyeVector=new THREE.Vector3();
		var addRays=[];
		addRays[0] =new THREE.Vector3(0, 0, 0);
		addRays[1] =new THREE.Vector3(0.08, 0.08, 0.08);
		addRays[2] =new THREE.Vector3(-0.08, 0.08, 0.08);
		addRays[3] =new THREE.Vector3(-0.08, -0.08, 0.08);
		addRays[4] =new THREE.Vector3(-0.08, -0.08, -0.08);
		addRays[5] =new THREE.Vector3(0.08, -0.08, -0.08);
		addRays[6] =new THREE.Vector3(0.08, 0.08, -0.08);
		addRays[7] =new THREE.Vector3(-0.08, 0.08, -0.08);
		addRays[8] =new THREE.Vector3(0.08, -0.08, 0.08);
		addRays[9] =new THREE.Vector3(0.015, 0.015, 0.015);
		addRays[10] =new THREE.Vector3(-0.015, 0.015, 0.015);
		addRays[11] =new THREE.Vector3(-0.015, -0.015, 0.015);
		addRays[12] =new THREE.Vector3(-0.015, -0.015, -0.015);
		addRays[13] =new THREE.Vector3(0.015, -0.015, -0.015);
		addRays[14] =new THREE.Vector3(0.015, 0.015, -0.015);
		addRays[15] =new THREE.Vector3(-0.015, 0.015, -0.015);
		addRays[16] =new THREE.Vector3(0.015, -0.015, 0.015);
		addRays[17] =new THREE.Vector3(0.025, 0.025, 0.025);
		addRays[18] =new THREE.Vector3(-0.025, 0.025, 0.025);
		addRays[19] =new THREE.Vector3(-0.025, 0, 0.025);
		addRays[20] =new THREE.Vector3(-0.025, 0, -0.025);
		addRays[21] =new THREE.Vector3(0.025, 0, -0.025);
		addRays[22] =new THREE.Vector3(0.025, 0.025, -0.025);
		addRays[23] =new THREE.Vector3(-0.025, 0.025, -0.025);
		addRays[24] =new THREE.Vector3(0.025, 0, 0.025);
		return function makeLightReflex (pointIncident, normalVector, objectIncident, infoRenderObject){
			if((infoRenderObject.shape ==1 || infoRenderObject.shape ==2  || infoRenderObject.shape == 0)){
				for ( var i = 0, l = lights.length; i < l; i ++ ) {

					var light = lights[ i ];
					for(var j = 0; j < addRays.length; j++ ){
						tmpVector.copy(pointIncident);
						tmpVector.add(addRays[j]);
						directionLight.copy(tmpVector);
						directionLight.sub(light.position);
						directionLight.normalize();
						reflectionLightVector.copy(directionLight);
						reflectionLightVector.reflect(normalVector);
						rayLight.origin.copy(tmpVector);
						rayLight.direction.copy(reflectionLightVector);
						
						var intersections = raycasterLight.intersectObjects( objects, true );

						// ray didn't find anything
						// (here should come setting of background color?)

						if ( intersections.length === 0 ) {
							continue;
						}

						// ray hit
						var intersection = intersections[ 0 ];
						if(intersection.distance > 1.5)
							continue;
						var point = intersection.point;
						var object = intersection.object;
						var material = object.material;
						var face = intersection.face;
						var _object=cache[object.uuid];
						
						
						if(infoRenderObject.shape ==0 && object.name.localeCompare("Plane") == 0 || infoRenderObject.shape ==1 && object.name.localeCompare("G_White_1") == 0)
							continue;
						pointp.set(point.x, point.y, point.z, 1);
						pointp.applyMatrix4(camera.matrixWorldInverse);
						pointp.applyMatrix4(camera.projectionMatrix);
						var x1=Math.round(SCREEN_WIDTH/2 + ((pointp.x/pointp.w)*SCREEN_WIDTH)/2);
						var y1=Math.round(SCREEN_HEIGHT/2 - ((pointp.y/pointp.w)*SCREEN_HEIGHT)/2);
						var ilr=new THREE.InfoLightReflex();
						ilr.xCaustics=x1;
						ilr.yCaustics=y1;
						eyeVector.subVectors(rayLight.origin, point).normalize();
						tmpVector.copy(reflectionLightVector);
						tmpVector.sub(point);
						tmpVector.normalize();
						localPointL.copy( point ).applyMatrix4( _object.inverseMatrix );
						calcColor(eyeVector, face.normal.applyMatrix3( _object.normalMatrix ).normalize(), object, infoRenderObject.textureColor, ilr.colorCausticsCT, light.intensity, tmpVector);
						
						infoRenderObject.infoLReflex.push(ilr);
					}
				}
			}
		}
	}() );
	
	var calcColorAllLight = ( function () {
		var lightVector=new THREE.Vector3();
		return function calcColorAllLight(eyeVector, normalVector, object, tmpTextureColor, outputColor, pointIntersection){
				
				// compute light shading
			for ( var i = 0, l = lights.length; i < l; i ++ ) {

				var light = lights[ i ];
				
				lightVector.setFromMatrixPosition( light.matrixWorld );
	//				lightVector.sub( point );

				// point visible
				lightVector.sub(pointIntersection);
				lightVector.normalize();
				calcColor(eyeVector, normalVector, object, tmpTextureColor, outputColor, light.intensity, lightVector);
			}
		}
	}() );
	

	var calcColor = ( function () {
		var halfVector=new THREE.Vector3();
		return function calcColor(eyeVector, normalVector, object, tmpTextureColor, outputColor, lightIntensity, lightVector){
				var lambertian = Math.max( normalVector.dot( lightVector ), 0.0001 );
	// model Cook–Torrance for specular light
	// m - m is the rms slope of the surface microfacets (the roughness of the material), we believe that m from interval (0, 1)
				halfVector.addVectors(lightVector, eyeVector).normalize();
				var halfCosAngle=Math.max( normalVector.dot( halfVector ), 0.0001 );
				var colorCT;
	// model Cook–Torrance for specular light
	// Shielding and Shading, microfacets
				var Gm=(2.0*(normalVector.dot(halfVector))*(normalVector.dot(lightVector)))/halfVector.dot(lightVector);
				var Gs=(2.0*(normalVector.dot(halfVector))*(normalVector.dot(eyeVector)))/halfVector.dot(lightVector);
				var G=Math.min(1.0, Math.min(Gm, Gs));
				var eyeCosAngle=Math.max( normalVector.dot( eyeVector ), 0.0001 );
				if(object.name.localeCompare("G_White_1") == 0 || object.name.localeCompare("D_White_1") == 0){
	// Beckmann distribution
					beckmann=distributionBeckmann(halfCosAngle, 0.17);
	// model Cook–Torrance for light in point
					colorCT=modelCookTorance(tmpTextureColor, goldAmbient, goldDiffuse, goldSpecular, goldIor, lambertian, beckmann, G, eyeCosAngle, lightIntensity);
					colorCT.multiplyScalar(goldShininess);
				}	
				if(object.name.localeCompare("Plane") == 0){
	// Beckmann distribution
					beckmann=distributionBeckmann(halfCosAngle, 0.6);
	// model Cook–Torrance for light in point
					colorCT=modelCookTorance(tmpTextureColor, planeAmbient, tmpTextureColor, planeSpecular, planeIor, lambertian, beckmann, G, eyeCosAngle, lightIntensity);
					colorCT.multiplyScalar(planeShininess);
				}
				outputColor.add(colorCT);

			}
	}() );
		
		function distributionBeckmann(specCosAngle, roughness){
			var specAngle=Math.acos(specCosAngle);
			var m=roughness;
			var beckmann=(Math.exp(-Math.pow((Math.tan(specAngle)/m), 2.0)))/(Math.PI*m*m*Math.pow(specCosAngle,4.0));
			return beckmann;
		}
		
		function modelCookTorance(color, ambient, diffuse, specular, ior, lambertian, beckmann, G, eyeCosAngle, intensity){
			var const_sun=0.00035;
			var ambientLight = scene.getObjectByName("ambient");
			if(eyeCosAngle == 0.0001) G=0;
			var eta_red=(1.0 + Math.sqrt(ior.r))/(1.0-Math.sqrt(ior.r));
			var g_red=Math.sqrt(eta_red*eta_red + lambertian*lambertian - 1.0);
			var fresnel_red2=(1.0 + Math.pow(((lambertian*(g_red+lambertian)-1.0))/((lambertian*(g_red-lambertian)-1.0)),2.0));
			var fresnel_red1=0.5*Math.pow((g_red - lambertian),2.0)/Math.pow((g_red + lambertian),2.0);
			var fresnel_red=fresnel_red1*fresnel_red2;
			var Ired=color.r*(0.1*ambientLight.intensity*ambient.r*ior.r + intensity*diffuse.r*const_sun*ior.r*lambertian+ intensity*specular.r*const_sun*fresnel_red*beckmann*G/eyeCosAngle);
			var eta_green=(1.0 + Math.sqrt(ior.g))/(1.0-Math.sqrt(ior.g));
			var g_green=Math.sqrt(eta_green*eta_green + lambertian*lambertian - 1.0);
			var fresnel_green2=(1.0 + Math.pow(((lambertian*(g_green+lambertian)-1.0))/((lambertian*(g_green-lambertian)-1.0)),2.0));
			var fresnel_green1=0.5*Math.pow((g_green - lambertian),2.0)/Math.pow((g_green + lambertian),2.0);
			var fresnel_green=fresnel_green1*fresnel_green2;
			var Igreen=color.g*(0.1*ambientLight.intensity*ambient.g*ior.g + intensity*diffuse.g*const_sun*ior.g*lambertian+ intensity*specular.g*const_sun*fresnel_green*beckmann*G/eyeCosAngle);
			var eta_blue=(1.0 + Math.sqrt(ior.b))/(1.-Math.sqrt(ior.b));
			var g_blue=Math.sqrt(eta_blue*eta_blue + lambertian*lambertian - 1.0);
			var fresnel_blue2=(1.0 + Math.pow(((lambertian*(g_blue+lambertian)-1.0))/((lambertian*(g_blue-lambertian)-1.0)),2.0));
			var fresnel_blue1=0.5*Math.pow((g_blue - lambertian),2.0)/Math.pow((g_blue + lambertian),2.0);
			var fresnel_blue=fresnel_blue1*fresnel_blue2;
			var Iblue=color.b*(0.1*ambientLight.intensity*ambient.b*ior.b + intensity*diffuse.b*const_sun*ior.b*lambertian+ intensity*specular.b*const_sun*fresnel_blue*beckmann*G/eyeCosAngle);
			return new THREE.Color(Ired,Igreen,Iblue);
		}
			var computePixelNormal = ( function () {

				var tmpVec1 = new THREE.Vector3();
				var tmpVec2 = new THREE.Vector3();
				var tmpVec3 = new THREE.Vector3();

				return function computePixelNormal( outputVector, point, shading, face, vertices ) {

					var faceNormal = face.normal;
					var vertexNormals = face.vertexNormals;

					if ( shading === THREE.FlatShading ) {

						outputVector.copy( faceNormal );

					} else if ( shading === THREE.SmoothShading ) {

						// compute barycentric coordinates
		//	console.log( 'computePixelNormal, count vertices = ' + vertices.length + ' face ' + face.a+ ', '+face.b + ', '+face.c+ ' face.vertexNormals = '+face.vertexNormals.length );

						var vA = vertices[ face.a ];
						var vB = vertices[ face.b ];
						var vC = vertices[ face.c ];

						tmpVec3.crossVectors( tmpVec1.subVectors( vB, vA ), tmpVec2.subVectors( vC, vA ) );
						var areaABC = faceNormal.dot( tmpVec3 );

						tmpVec3.crossVectors( tmpVec1.subVectors( vB, point ), tmpVec2.subVectors( vC, point ) );
						var areaPBC = faceNormal.dot( tmpVec3 );
						var a = areaPBC / areaABC;

						tmpVec3.crossVectors( tmpVec1.subVectors( vC, point ), tmpVec2.subVectors( vA, point ) );
						var areaPCA = faceNormal.dot( tmpVec3 );
						var b = areaPCA / areaABC;

						var c = 1.0 - a - b;

						// compute interpolated vertex normal

						tmpVec1.copy( vertexNormals[ 0 ] );
						tmpVec1.multiplyScalar( a );

						tmpVec2.copy( vertexNormals[ 1 ] );
						tmpVec2.multiplyScalar( b );

						tmpVec3.copy( vertexNormals[ 2 ] );
						tmpVec3.multiplyScalar( c );

						outputVector.addVectors( tmpVec1, tmpVec2 );
						outputVector.add( tmpVec3 );

					}

				};

			}() );

			


}
Object.assign( THREE.WEBGLRaytracingRendererWorker.prototype, THREE.EventDispatcher.prototype );

