/*
Inna Daymand, 2018, GaazIT

structures for data for rendering
*/

THREE.InfoLightReflex = class{
	constructor(){
		this.xCaustics=0; this.yCaustics=0;
		this.colorCausticsCT=new THREE.Color(0,0,0);
	}
};

THREE.InfoInflectionPoint = class {
	constructor(){
		this.xInfPoint=0, this.yInfPoint=0;
		this.sinAngle=0;
		this.colorInfPoint=new THREE.Color(0,0,0);
	}
};

THREE.InfoEdge = class{
	constructor(){
		this.edge=new THREE.Vector3();
	}
};

THREE.InfoFace = class {
	constructor(){
		this.indexVert=new Array();
		this.indexUVert=new Array();
		this.edge1=new THREE.InfoEdge();
		this.edge2=new THREE.InfoEdge();
		this.normal=new THREE.Vector3();
	}
};

THREE.InfoUVertices = class {
	constructor(){
		this.vertice=new THREE.Vector3();
		this.indices=new Array();
		this.faces=new Array();
	}
};

THREE.InfoRendering = class{
	// 0 - plane, 1 - ring, 2 - diamond
	constructor(){
		this.xShape=0; this.yShape=0;
		this.colorShape=new THREE.Color(0,0,0);
		this.shape=-1; 
		this.count=0;
		this.indexU=-1; // index of unique vertice in array of unique vertices
		this.textureColor=new THREE.Color(0,0,0);
		this.uvCoordinates=new THREE.Vector2();
		this.infoLReflex=new Array();
		this.calcPoint=false;
		this.point=new THREE.Vector3();
		this.normal=new THREE.Vector3();
		this.onBorder=false;
		this.onContour=false;
		this.onBorder1=false;
		this.infoInflectionPoints=new Array();
		this.normTextureColor=0;
	};
	
	copyInfoLRefrex( source, xOffset, yOffset ) {
		for(var i=0; i < source.infoLReflex.length; i++){
			var src=source.infoLReflex[i];
			var dst=new THREE.InfoLightReflex();
			dst.xCaustics=src.xCaustics + xOffset;
			dst.yCaustics=src.yCaustics + yOffset;
			dst.colorCausticsCT.copy(src.colorCausticsCT);
			this.infoLReflex.push(dst);
		}
		return this;
	};
	
	copy(source){
		this.colorShape.copy(source.colorShape);
		this.xShape=source.xShape;
		this.yShape=source.yShape;
		this.shape=source.shape;
		this.point.copy(source.point);
		this.uvCoordinates.copy(source.uvCoordinates);
		this.normal.copy(source.normal);
		this.normTextureColor=source.normTextureColor;
	};
	
};

THREE.InfoRenderingIntersection = class {
	constructor(){
		this.infor=new THREE.InfoRendering();
		this.intersection=new THREE.InfoIntersection();
	}
}

THREE.InfoIntersection = class {
	constructor(){
		this.point=new THREE.Vector3();
		this.uvCoordinates=new THREE.Vector2();
		this.object=null;
		this.face=null;
	}
}

THREE.InfoRenderingBlock = class{
	constructor(){
		this.numBlockX=-1;
		this.numBlockY=-1;
		this.arrayInfoRendering=new Array();
	}
	
};
