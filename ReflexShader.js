/*
Inna Daymand, 2018, GaazIT

ReflexShader

input texture: cubeCameraObject.renderTarget.texture
			or cubeCameraFloor.renderTarget.texture
			

*/

ReflexShader = {

	uniforms: {
    "resolution": {
      "name": "resolution",
      "type": "v2",
      "glslType": "vec2",
      "description": ""
    },
    "lightPosition": {
      "name": "lightPosition",
      "type": "v3",
      "glslType": "vec3",
      "description": ""
    },
	"u_texture" : {
		"name" :"u_texture",
		"value" : null
	},
	"ior": { 	//index of refraction
		"name" : "ior",
		"type": "v3",
		"glslType" : "vec3",
		"value": "", 
	},
	"fresnelBias": { //parameter for reflection model
		"value": "0.01" 
	},
	"fresnelPower": { //parameter for reflection model 
		"value": "2.0" 
	},
	"fresnelScale": { //parameter for reflection model
		"value": ".5" 
	},
	},

	vertexShader: [
"precision highp float;",
"precision highp int;",

"uniform mat4 modelMatrix;", //= object.MatrixWorld
"uniform mat4 modelViewMatrix;", // = camera.matrixWorldInverse*object.matrixWorld
"uniform mat4 projectionMatrix;", // = camera.projectionMatrix
"uniform mat4 viewMatrix;", // = camera.matrixWorldInverse
"uniform mat3 normalMatrix;", // = inverse transpose of modelViewMatrix
"uniform vec3 cameraPosition;", // = camera position in world space

"attribute vec3 position;",
"attribute vec3 normal;",
"attribute vec2 uv;",

"uniform vec2 resolution;",
"uniform vec3 lightPosition;",
"uniform vec3 ior;",
"uniform float fresnelBias;",
"uniform float fresnelPower;",
"uniform float fresnelScale;",

"varying vec3 vReflect;",
"varying vec3 vRefract[3];",
"varying float vReflectionFactor;",
"varying vec3 vWorldPosition;",

"void main() {",

"	 vec3 lNormal=normalize(mat3(normalMatrix)*normal);",
    // This sets the position of the vertex in 3d space. The correct math is
    // provided below to take into account camera and object data.
"	 vec4 mv_position=modelViewMatrix * vec4( position, 1.0 );",
"	vec4 worldPosition = modelMatrix * vec4( position, 1.0 );",
"	vWorldPosition =worldPosition.xyz; ",

"	vec3 worldNormal = normalize( mat3( modelMatrix[0].xyz, modelMatrix[1].xyz, modelMatrix[2].xyz ) * normal );",

"	vec3 I = worldPosition.xyz - cameraPosition;",
"	vec3 L = worldPosition.xyz - lightPosition;",
"	vec3 D = normalize(I);",

"	vReflect = reflect( D, worldNormal );",
"	vRefract[0] = refract( normalize( D ), worldNormal, ior.r );",
"	vRefract[1] = refract( normalize( D ), worldNormal, ior.g );",
"	vRefract[2] = refract( normalize( D ), worldNormal, ior.b );",
"	vReflectionFactor = fresnelBias + fresnelScale * pow( 1.0 + dot( normalize( D ), worldNormal ), fresnelPower );",
"   gl_Position = projectionMatrix * mv_position;",

"}"

	].join( "\n" ),

	fragmentShader: [
	"precision highp float;",


// The texture result of rendering step 1.

	"uniform samplerCube u_texture;",

	"varying vec3 vReflect;",
	"varying vec3 vRefract[3];",
	"varying float vReflectionFactor;",
	"varying vec3 vWorldPosition;",
	
// constants	
	"#define pi 3.14159275358979", 
	"#define EPS 4e-4",

"void main() {",

	
"	vec4 reflectedColor= textureCube(u_texture, vec3(vReflect.x, vReflect.y, vReflect.z));",
"	vec4 refractedColor = vec4( 1.0 );",

"	refractedColor.r = textureCube( u_texture, vec3( vRefract[0].x, vRefract[0].yz ) ).r;",
"	refractedColor.g = textureCube( u_texture, vec3( vRefract[1].x, vRefract[1].yz ) ).g;",
"	refractedColor.b = textureCube( u_texture, vec3( vRefract[2].x, vRefract[2].yz ) ).b;",

"	gl_FragColor = mix( refractedColor, reflectedColor, (1.0- clamp( vReflectionFactor, 0.0, 1.0 )) );",

"}"
	].join( "\n" )

};
