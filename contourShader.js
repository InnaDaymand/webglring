/*
Inna Daymand, 2018, GaazIT

contourShader

input texture: postprocessing.rtBlurringTexture.texture

make contours of object ring
*/

contourShader = {

	uniforms: {
    "resolution": {
      "name": "resolution",
      "type": "v2",
      "glslType": "vec2",
      "description": ""
    },
    "lightPosition": {
      "name": "lightPosition",
      "type": "v3",
      "glslType": "vec3",
      "description": ""
    },
	"uBasis" : {
		"name" :"uBasis",
		"value" : null
	},
	"redBalance" : {
		"name" : "redBalance",
		"type": "f",
		"glslType": "float",
		"value": "0.0"
	},
	},
	vertexShader: [
"precision highp float;",
"precision highp int;",

"uniform mat4 modelMatrix;", //= object.MatrixWorld
"uniform mat4 modelViewMatrix;", // = camera.matrixWorldInverse*object.matrixWorld
"uniform mat4 projectionMatrix;", // = camera.projectionMatrix
"uniform mat4 viewMatrix;", // = camera.matrixWorldInverse
"uniform mat3 normalMatrix;", // = inverse transpose of modelViewMatrix
"uniform vec3 cameraPosition;", // = camera position in world space

"attribute vec3 position;",
"attribute vec3 normal;",
"attribute float shape;",
"attribute vec2 uv;",
"attribute float isContour;",
"attribute float isBorder;",
"attribute float isBorder1;",
"attribute float randomValue;",

"uniform vec3 lightPosition;",
"uniform vec2 resolution;",


"varying vec3 vWorldPosition;",
"varying vec3 vWorldNormal;",
"varying vec2 vUv;",
"varying float vShape;",
"varying vec2 vtex_coord;",
"varying float vIsContour;",
"varying float vIsBorder;",
"varying float vIsBorder1;",
"varying float vRandomValue;",

"void main() {",

    // This sets the position of the vertex in 3d space. The correct math is
    // provided below to take into account camera and object data.
"	vec4 mv_position=modelViewMatrix * vec4( position, 1.0 );",
"	vec4 worldPosition = modelMatrix * vec4( position, 1.0 );",
"	vWorldPosition =worldPosition.xyz; ",

"	vWorldNormal = normalize( mat3( modelMatrix[0].xyz, modelMatrix[1].xyz, modelMatrix[2].xyz ) * normal );",

"	 vIsContour = isContour;",     
"	 vIsBorder = isBorder;",     
"	 vIsBorder1 = isBorder1;",     

"   gl_Position = projectionMatrix * mv_position;",
// 	getting UV coordinates of position in input texture
"	float x1 = ((gl_Position.x/gl_Position.w)+1.0)*resolution.x/2.0;",
"	float y1 = ((gl_Position.y/gl_Position.w)+1.0)*resolution.y/2.0;",
"	vUv.x = (x1)/resolution.x;",
"	vUv.y = (y1)/resolution.y;",
//---------------------------------------------------------
"	vShape=shape;",
"	vtex_coord=uv;",
"	vRandomValue=randomValue;",
"}"

	].join( "\n" ),

	fragmentShader: [
"precision highp float;",

	
"uniform sampler2D uBasis;",
"uniform vec2 resolution;",
"uniform float redBalance;",


"varying vec3 vWorldPosition;",
"varying vec3 vWorldNormal;",
"varying vec2 vUv;",
"varying float vShape;",
"varying vec2 vtex_coord;",
"varying float vIsContour;",
"varying float vIsBorder;",
"varying float vIsBorder1;",
"varying float vRandomValue;",


//model of contour.
//use aperture 5 left, 5 right (radius)
"float normColor(vec3 color){",
"	return (color.r+color.g+color.b);",
"}",

"const int radius=5;",

// calculation of variable for model
"float calcX(int y,  float offset_x){",
"	vec3 color=texture2D(uBasis, vUv).rgb;",
"	vec2 onePixel=vec2(1.0, 1.0)/resolution;",
"	float avgNorm=normColor(color);",
"	float count=1.0;",
"	for(int x= -1; x > -radius -1 ; x--){",
"		color=texture2D(uBasis, vec2(vUv.x + offset_x, vUv.y) + onePixel*vec2(x, y)).rgb;	",
"		avgNorm=avgNorm+normColor(color);",
"		count+=1.0;",
"	}",
"	for(int x= 1; x <radius +1 ; x++){",
"		color=texture2D(uBasis, vec2(vUv.x + offset_x, vUv.y) + onePixel*vec2(x, y)).rgb;	",
"		avgNorm=avgNorm+normColor(color);",
"		count+=1.0;",
"	}",
"	avgNorm=avgNorm/count;",
"	return avgNorm;",
"}",

//normalization
"float normFactorColor(float normColor){",
"	return ((2.0*normColor - 3.5)/2.5); 	",// 0.5 ---- 3.0
"}",
"",

//set new color by finally
"vec3 setNewColor(vec3 inout_color, float norm){",
"	if(norm >=2.99){",
"		inout_color.r =1.0;",
"		inout_color.g =1.0;",
"		inout_color.b =1.0;",
"		return inout_color;",
"	}",
"	float oldNorm=normColor(inout_color);",
"	if((oldNorm) == 0.0)",
"		return vec3(0.0);",
"	float newRED=(inout_color.r*norm)/(oldNorm);",
"	float newGREEN=(inout_color.g*norm)/(oldNorm);",
"	float newBLUE=(inout_color.b*norm)/(oldNorm);",
"	newRED=newRED>1.0?1.0:newRED;",
"	newGREEN=newGREEN>1.0?1.0:newGREEN;",
"	newBLUE=newBLUE>1.0?1.0:newBLUE;",
"	inout_color.r=newRED;",
"	inout_color.g=newGREEN;",
"	inout_color.b=newBLUE;",
"	return inout_color;",
"}",
	
/*
							
"	float y=1.9702+0.03418*X1+0.0343*X3+0.11242*X4+0.18098*X5-0.2102*X6-0.0343*X1*X3+0.17098*X1*X6+0.0243*X2*X3+0.06336*X2*X4+0.1516*X2*X6+0.06355*X3*X4+0.03406*X3*X5+0.21*X3*X6+",
"		0.05375*X4*X5+0.22922*X4*X6+0.25871*X5*X6+0.03398*X1*X2*X3+0.0341*X1*X3*X4-0.03422*X1*X3*X6-0.05359*X1*X4*X6-0.04387*X1*X5*X6+0.04387*X3*X4*X5-",
"		0.03418*X3*X4*X6-0.0243*X3*X5*X6-0.02477*X4*X5*X6	;	",
														
"	float y=1.8702+0.03418*X1+0.0343*X3+0.11242*X4+0.18098*X5-0.2102*X6-0.0343*X1*X3+0.17098*X1*X6+0.0243*X2*X3+0.06336*X2*X4+0.1516*X2*X6+0.06355*X3*X4+0.03406*X3*X5+0.21*X3*X6+",
"		0.05375*X4*X5+0.22922*X4*X6+0.25871*X5*X6+0.03398*X1*X2*X3+0.0341*X1*X3*X4-0.03422*X1*X3*X6-0.05359*X1*X4*X6-0.04387*X1*X5*X6+0.04387*X3*X4*X5-",
"		0.03418*X3*X4*X6-0.0243*X3*X5*X6-0.02477*X4*X5*X6	;	",
*/	
	
// calculation of factors, and calculation of models for contour
"vec3 applyMyFilterContour(){",
"	vec3 color=texture2D(uBasis, vUv).rgb;",
"	float norm=normColor(color);",
"	float x1_f = calcX(-1, 0.0);",
"	float x2_f =calcX(0,  0.0);",
"	float x3_f =calcX(1,  0.0);",
"	float x4_f =calcX(-2,  0.0);",
"	float x5_f =calcX(2,  0.0);",
"	float X1=normFactorColor(x1_f);",
"	float X2=normFactorColor(x2_f);",
"	float X3=normFactorColor(x3_f);",
"	float X4=normFactorColor(x4_f);",
"	float X5=normFactorColor(x5_f);",
"	float X6=0.0;",
"	float y=1.8702+0.03418*X1+0.0343*X3+0.11242*X4+0.18098*X5-0.2102*X6-0.0343*X1*X3+0.17098*X1*X6+0.0243*X2*X3+0.06336*X2*X4+0.1516*X2*X6+0.06355*X3*X4+0.03406*X3*X5+0.21*X3*X6+",
"		0.05375*X4*X5+0.22922*X4*X6+0.25871*X5*X6+0.03398*X1*X2*X3+0.0341*X1*X3*X4-0.03422*X1*X3*X6-0.05359*X1*X4*X6-0.04387*X1*X5*X6+0.04387*X3*X4*X5-",
"		0.03418*X3*X4*X6-0.0243*X3*X5*X6-0.02477*X4*X5*X6	;	",
"	y = y >2.99? 3.0 : y;",
"	return setNewColor(color, (y)/1.0);",
"}",

// calculation of factors, and calculation of models for border
"vec3 applyMyFilterBorder(){",
"	vec3 color=texture2D(uBasis, vUv).rgb;",
"	float norm=normColor(color);",
"	float x1_f = calcX(-1, 0.0);",
"	float x2_f =calcX(0,  0.0);",
"	float x3_f =calcX(1,  0.0);",
"	float x4_f =calcX(-2,  0.0);",
"	float x5_f =calcX(2,  0.0);",
"	float X1=normFactorColor(x1_f);",
"	float X2=normFactorColor(x2_f);",
"	float X3=normFactorColor(x3_f);",
"	float X4=normFactorColor(x4_f);",
"	float X5=normFactorColor(x5_f);",
"	float X6=-1.0;",
"	float y=1.8702+0.03418*X1+0.0343*X3+0.11242*X4+0.18098*X5-0.2102*X6-0.0343*X1*X3+0.17098*X1*X6+0.0243*X2*X3+0.06336*X2*X4+0.1516*X2*X6+0.06355*X3*X4+0.03406*X3*X5+0.21*X3*X6+",
"		0.05375*X4*X5+0.22922*X4*X6+0.25871*X5*X6+0.03398*X1*X2*X3+0.0341*X1*X3*X4-0.03422*X1*X3*X6-0.05359*X1*X4*X6-0.04387*X1*X5*X6+0.04387*X3*X4*X5-",
"		0.03418*X3*X4*X6-0.0243*X3*X5*X6-0.02477*X4*X5*X6	;	",
"	y = y >2.99? 3.0 : y;",
"	return setNewColor(color, (y)/1.0);",
"}",

// calculation of factors, and calculation of models for border1
"vec3 applyMyFilterBorder1(){",
"	vec3 color=texture2D(uBasis, vUv).rgb;",
"	float norm=normColor(color);",
"	float x1_f = calcX(-1, 0.0);",
"	float x2_f =calcX(0,  0.0);",
"	float x3_f =calcX(1,  0.0);",
"	float x4_f =calcX(-2,  0.0);",
"	float x5_f =calcX(2,  0.0);",
"	float X1=normFactorColor(x1_f);",
"	float X2=normFactorColor(x2_f);",
"	float X3=normFactorColor(x3_f);",
"	float X4=normFactorColor(x4_f);",
"	float X5=normFactorColor(x5_f);",
"	float X6=1.0;",
"	float y=1.8702+0.03418*X1+0.0343*X3+0.11242*X4+0.18098*X5-0.2102*X6-0.0343*X1*X3+0.17098*X1*X6+0.0243*X2*X3+0.06336*X2*X4+0.1516*X2*X6+0.06355*X3*X4+0.03406*X3*X5+0.21*X3*X6+",
"		0.05375*X4*X5+0.22922*X4*X6+0.25871*X5*X6+0.03398*X1*X2*X3+0.0341*X1*X3*X4-0.03422*X1*X3*X6-0.05359*X1*X4*X6-0.04387*X1*X5*X6+0.04387*X3*X4*X5-",
"		0.03418*X3*X4*X6-0.0243*X3*X5*X6-0.02477*X4*X5*X6	;	",
"	y = y >2.99? 3.0 : y;",
"	return setNewColor(color, (y)/1.0);",
"}",

//color balance
//----------------------------------------------------------
"	vec3 colorRedBalance(vec3 color){",
"		color.b=color.b - redBalance;",
"		return color;",
"",
"	}",

"float rand(vec2 v){",
"	return (vRandomValue*(v.y - v.x) + v.x);",
"",
"",
"}",
//color balance
"	vec3 colorBalance(vec3 color){",
"		if(color.g < color.b)",
"			color.b=color.g;",
"		float maxColorNorm=2.98;",
"		float kNorm=1.0/255.0;",
"		float min_drg=10.0;",
"		float max_drg=30.0;",
"		if(normColor(color) >=maxColorNorm)",
"			return color;",
"		float drg=color.r-color.g;",
"		if(drg < min_drg*kNorm){",
"			color.g=color.g-rand(vec2(min_drg, min_drg+5.0))*kNorm;",
"		}",
"		if(drg >max_drg*kNorm){",
"			drg=drg-(max_drg - 5.0)*kNorm;",
"			color.r=color.r-drg;",
"		}",
"		float min_dgb=30.0;",
"		float max_dgb=50.0;",
"		float limit_dgb=100.0;",
"		float dgb=color.g-color.b;",
"		if(dgb >min_dgb*kNorm && dgb < max_dgb*kNorm){",
"			dgb=dgb-rand(vec2(min_dgb - 5.0, min_dgb))*kNorm;",
"			color.b=color.b +dgb;",
"		}",
"		if(dgb >=max_dgb*kNorm && dgb < limit_dgb*kNorm){",
"			color.b=color.b +dgb*0.8;",
"		}",
"		if(dgb >= limit_dgb*kNorm){",
"			color.b=color.g -  rand(vec2(min_drg, min_dgb))*kNorm;",
"		}",
"		if(dgb < min_drg*kNorm && dgb > 0.0){",
"			color.b=color.b - rand(vec2(min_drg, min_drg+5.0))*kNorm;",
"		}",
"		if(dgb <= 0.0){",
"			color.b=color.g - rand(vec2(min_dgb, min_dgb+5.0))*kNorm;",
"		}",
//"		color=colorRedBalance(color);",
"		return color;",
"	}",
//---------------------------------------------------------------------------------------

"void main() {",
"	if(vShape > 0.98 && vShape < 1.98){",	
"		vec3 colorC=vec3(0.0);",
"		vec3 colorB=vec3(0.0);",
"		vec3 colorB1=vec3(0.0);",
"		float count=0.0;",
"		if( vIsContour > 0.98){",
"			colorC=applyMyFilterContour();",
"				colorC=mix(colorC, colorBalance(colorC), 1.0);",
"				colorC=colorC*1.2; ",
"				colorC=mix(colorC, vec3(200.0/255.0, 185.0/255.0, 160.0/255.0), 0.25); ",
"				count=count+1.0;",
"		}",
"		if(vIsBorder > 0.98 ){",
"			colorB=applyMyFilterBorder();",
"				colorB=mix(colorB, colorBalance(colorB), 1.0);",
"				colorB=colorB*1.1; ",
"				colorB=mix(colorB, vec3(225.0/255.0, 185.0/255.0, 160.0/255.0), 0.35); ",
"				count=count+1.0;",
"		}",
"		if(vIsBorder1 > 0.98 ){",
"			colorB1=applyMyFilterBorder1();",
"				colorB1=mix(colorB1, colorBalance(colorB1), 1.0);",
"				colorB1=colorB1*0.65; ",
"				count=count+1.0;",
"		}",
"		gl_FragColor = vec4((1.0/count)*((colorC+colorB+colorB1).xyz), 1.0);",
"	}",
"	if((vShape > 0.98 && vShape <1.98 && vIsContour < 0.98 && vIsBorder < 0.98 && vIsBorder1 < 0.98) || vShape < 0.98 || vShape >1.98){",
"		gl_FragColor=texture2D(uBasis, vUv);",
"	}",
"}",
	].join( "\n" )

};

