/*
Inna Daymand, 2018, GaazIT

contrastShader

input texture: postprocessing.rtUnionTexture.texture

make contrast of ring

*/

contrastShader = {

	uniforms: {
    "resolution": {
      "name": "resolution",
      "type": "v2",
      "glslType": "vec2",
      "description": ""
    },
    "lightPosition": {
      "name": "lightPosition",
      "type": "v3",
      "glslType": "vec3",
      "description": ""
    },
	"uBasis" : {
		"name" :"uBasis",
		"value" : null
	},
	"contrast":{
		"name" : "contrastGold", 
	},
	"saturation" : {
		"name" : "saturation",
	},
	"_vibrance" : {
		"name" : "_vibrance",
	},
	"_hue" : {
		"name" : "_hue",
	},
	"brightness" : {
		"name" : "brightness",
	},
	},

	vertexShader: [
"precision highp float;",
"precision highp int;",

"uniform mat4 modelMatrix;", //= object.MatrixWorld
"uniform mat4 modelViewMatrix;", // = camera.matrixWorldInverse*object.matrixWorld
"uniform mat4 projectionMatrix;", // = camera.projectionMatrix
"uniform mat4 viewMatrix;", // = camera.matrixWorldInverse
"uniform mat3 normalMatrix;", // = inverse transpose of modelViewMatrix

"attribute vec3 position;",
"attribute vec3 normal;",
"attribute float shape;",
"attribute float isContour;",
"attribute float isBorder;",
"attribute float randomValue;",

"uniform vec3 lightPosition;",
"uniform vec2 resolution;",


"varying vec3 vWorldPosition;",
"varying vec3 vWorldNormal;",
"varying vec2 vUv;",
"varying float vShape;",
"varying float vIsContour;",
"varying float vIsBorder;",
"varying float vRandomValue;",

"void main() {",

    // This sets the position of the vertex in 3d space. The correct math is
    // provided below to take into account camera and object data.
"	vec4 mv_position=modelViewMatrix * vec4( position, 1.0 );",
"	vec4 worldPosition = modelMatrix * vec4( position, 1.0 );",
"	vWorldPosition =worldPosition.xyz; ",

"	vWorldNormal =  mat3( modelMatrix[0].xyz, modelMatrix[1].xyz, modelMatrix[2].xyz ) * normal ;",
"   gl_Position = projectionMatrix * mv_position;",
// 	getting UV coordinates of position in input texture
"	float x1 = ((gl_Position.x/gl_Position.w)+1.0)*resolution.x/2.0;",
"	float y1 = ((gl_Position.y/gl_Position.w)+1.0)*resolution.y/2.0;",
"	vUv.x = (x1)/resolution.x;",
"	vUv.y = (y1)/resolution.y;",
//------------------------------------------------------------------
"	vShape=shape;",
"	vIsContour=isContour;",
"	vIsBorder=isBorder;",
"	vRandomValue=randomValue;",

"}"

	].join( "\n" ),

	fragmentShader: [
	"precision highp float;",

// postprocessing.rtUnionTexture.texture	
	"uniform sampler2D uBasis;",
	
// can be changed by the user
	"uniform float contrast;",
	"uniform float saturation;",
	"uniform float brightness;",
	"uniform float _vibrance;",
	"uniform float _hue;",
//-----------------------------	
	"varying vec2 vUv;",
	"varying vec3 vWorldPosition;",
	"varying vec3 vWorldNormal;",
	"varying float vShape;",
	"varying float vIsContour;",
	"varying float vIsBorder;",
	"varying float vRandomValue;",
	
"float normColor(vec3 color){",
"	return (color.r+color.g+color.b);",
"}",

// not use red balans in this shader
//"	vec3 colorRedBalance(vec3 color){",
//"		color.b=color.b - redBalance;",
//"		return color;",
//"",
//"	}",

"float rand(vec2 v){",
"	return (vRandomValue*(v.y - v.x) + v.x);",
"",
"",
"}",
//color balance
"	vec3 colorBalance(vec3 color){",
"		if(color.g < color.b)",
"			color.b=color.g;",
"		float maxColorNorm=2.98;",
"		float kNorm=1.0/255.0;",
"		float min_drg=10.0;",
"		float max_drg=30.0;",
"		if(normColor(color) >=maxColorNorm)",
"			return color;",
"		float drg=color.r-color.g;",
"		if(drg < min_drg*kNorm){",
"			color.g=color.g-rand(vec2(min_drg, min_drg+5.0))*kNorm;",
"		}",
"		if(drg >max_drg*kNorm){",
"			drg=drg-(max_drg - 5.0)*kNorm;",
"			color.r=color.r-drg;",
"		}",
"		float min_dgb=30.0;",
"		float max_dgb=50.0;",
"		float limit_dgb=100.0;",
"		float dgb=color.g-color.b;",
"		if(dgb >min_dgb*kNorm && dgb < max_dgb*kNorm){",
"			dgb=dgb-rand(vec2(min_dgb - 5.0, min_dgb))*kNorm;",
"			color.b=color.b +dgb;",
"		}",
"		if(dgb >=max_dgb*kNorm && dgb < limit_dgb*kNorm){",
"			color.b=color.b +dgb*0.8;",
"		}",
"		if(dgb >= limit_dgb*kNorm){",
"			color.b=color.g -  rand(vec2(min_drg, min_dgb))*kNorm;",
"		}",
"		if(dgb < min_drg*kNorm && dgb > 0.0){",
"			color.b=color.b - rand(vec2(min_drg, min_drg+5.0))*kNorm;",
"		}",
"		if(dgb <= 0.0){",
"			color.b=color.g - rand(vec2(min_dgb/2.0, min_dgb/2.0+5.0))*kNorm;",
"		}",
//"		color=colorRedBalance(color);",
"		return color;",
"	}",

"vec4 brightnessAdjust( vec4 color, in float b) {",
"    color.rgb += b;",
"	 return color;",
"}",
 
"vec4 contrastAdjust( vec4 color, in float c) {",
  // Get average value to grayscale
//"  float average = (color.r + color.g + color.b) / 3.0;",

  // Tween contrast.
//"  float contrastFactor = c + 0.5;",
//"  if (average > 0.5) {",
//"    average = min(average + (average - 0.5) * contrastFactor, 1.0);",
//"  } else {",
//"    average = max(average - (0.5 - average) * contrastFactor, 0.0);",
//  "}",
//"  float factor = abs(c);",
//"  float r = color.r - factor * (color.r - average);",
//"  float g = color.g - factor * (color.g - average);",
//"  float b = color.b - factor * (color.b - average);",
"    float t = 0.5 - c * 0.5; ",
"    color.rgb = color.rgb * c + t;",
"	 return color;",
//"	 return vec4(r, g, b, 1.0);",
"}",
 
"mat4 saturationMatrix( float saturation ) {",
"    vec3 luminance = vec3(0.24725, 0.1995, 0.0745);",
"    float oneMinusSat = 1.0 - saturation;",
"    vec3 red = vec3( luminance.x * oneMinusSat );",
"    red.r += saturation;",
    
"    vec3 green = vec3( luminance.y * oneMinusSat );",
"    green.g += saturation;",
    
"    vec3 blue = vec3( luminance.z * oneMinusSat );",
"    blue.b += saturation;",
    
"    return mat4( ",
"        red,     0,",
"        green,   0,",
"        blue,    0,",
"        0, 0, 0, 1 );",
"}",
 
"int modi(int x, int y) {",
"    return x - y * (x / y);",
"}",
 
"int and(int a, int b) {",
"    int result = 0;",
"    int n = 1;",
"	const int BIT_COUNT = 32;",
 
"    for(int i = 0; i < BIT_COUNT; i++) {",
"        if ((modi(a, 2) == 1) && (modi(b, 2) == 1)) {",
"            result += n;",
"        }",
 
"        a =a/ 2;",
"        b =b/ 2;",
"        n =n*2;",
 
"        if (!(a > 0 && b > 0))",
"            break;",
"    }",
"    return result;",
"}",
 
"vec4 vibrance(vec4 inCol, float vibrance)", //r,g,b 0.0 to 1.0,  vibrance 1.0 no change, 0.0 image B&W.
"{",
" 	vec4 outCol;",
"   if (vibrance <= 1.0)",
"    {",
"        float avg = dot(inCol.rgb, vec3(0.3, 0.6, 0.1));",
"        outCol.rgb = mix(vec3(avg), inCol.rgb, vibrance);",
"    }",
"    else", // vibrance > 1.0
"    {",
"        float hue_a, a, f, p1, p2, p3, i, h, s, v, amt, _max, _min, dlt;",
"        float br1, br2, br3, br4, br5, br2_or_br1, br3_or_br1, br4_or_br1, br5_or_br1;",
"        int use;",
 
"        _min = min(min(inCol.r, inCol.g), inCol.b);",
"        _max = max(max(inCol.r, inCol.g), inCol.b);",
"        dlt = _max - _min + 0.00001;",
"        h = 0.0;",
"        v = _max;",
 
"		br1 = step(_max, 0.0);",
"        s = (dlt / _max) * (1.0 - br1);",
"        h = -1.0 * br1;",
 
"		br2 = 1.0 - step(_max - inCol.r, 0.0); ",
"        br2_or_br1 = max(br2, br1);",
"        h = ((inCol.g - inCol.b) / dlt) * (1.0 - br2_or_br1) + (h*br2_or_br1);",
 
"		br3 = 1.0 - step(_max - inCol.g, 0.0); ",
        
"        br3_or_br1 = max(br3, br1);",
"        h = (2.0 + (inCol.b - inCol.r) / dlt) * (1.0 - br3_or_br1) + (h*br3_or_br1);",
 
"        br4 = 1.0 - br2*br3;",
"        br4_or_br1 = max(br4, br1);",
"        h = (4.0 + (inCol.r - inCol.g) / dlt) * (1.0 - br4_or_br1) + (h*br4_or_br1);",
 
"        h = h*(1.0 - br1);",
 
"        hue_a = abs(h);", // between h of -1 and 1 are skin tones
"        a = dlt;",      // Reducing enhancements on small rgb differences
 
        // Reduce the enhancements on skin tones.    
"        a = step(1.0, hue_a) * a * (hue_a * 0.67 + 0.33) + step(hue_a, 1.0) * a;",
"        a *= (vibrance - 1.0);",
"        s = (1.0 - a) * s + a * pow(abs(s), 0.25);",
 
"        i = floor(h);",
"        f = h - i;",
 
"        p1 = v * (1.0 - s);",
"        p2 = v * (1.0 - (s * f));",
"        p3 = v * (1.0 - (s * (1.0 - f)));",
 
"        inCol.rgb = vec3(0.0); ",
"        i += 6.0;",
"        use = int(pow(2.0,mod(i,6.0)));",
"        a = float(and(use , 1));", // i == 0;
"        use =use/ 2;",
"        inCol.rgb += a * vec3(v, p3, p1);",
 
"        a = float(and(use , 1));", // i == 1;
"        use = use/2;",
"        inCol.rgb += a * vec3(p2, v, p1); ",
 
"        a = float( and(use,1));", // i == 2;
"        use =use /2;",
"        inCol.rgb += a * vec3(p1, v, p3);",
 
"        a = float(and(use, 1));", // i == 3;
"        use =use/ 2;",
"        inCol.rgb += a * vec3(p1, p2, v);",
 
"        a = float(and(use, 1));", // i == 4;
"        use =use / 2;",

"        inCol.rgb += a * vec3(p3, p1, v);",
 
"        a = float(and(use, 1));", // i == 5;
"        use =use / 2;",
"        inCol.rgb += a * vec3(v, p1, p2);",
 
"        outCol = inCol;",
"    }",
"    return outCol;",
"}",
 
"vec4 shiftHue(in vec3 col, in float Shift)",
"{",
"    vec3 P = vec3(0.55735) * dot(vec3(0.55735), col);",
"    vec3 U = col - P;",
"    vec3 V = cross(vec3(0.55735), U);",
"    col = U * cos(Shift * 6.2832) + V * sin(Shift * 6.2832) + P;",
"    return vec4(col, 1.0);",
"}",

"void main() {",
"	vec4 basis=(texture2D(uBasis, vUv));",
"	float norm=normColor(basis.rgb);",
"	if(norm < 2.95 && vShape > 0.98 && vIsContour <0.98 && vIsBorder < 0.98){",
//"		float luminace= basis.r * 0.299 + basis.g * 0.587 + basis.b * 0.114;",
"		vec4 color=basis;",
"    	color=brightnessAdjust(color, brightness); ",
"    	color=contrastAdjust(color, contrast); ",
"		gl_FragColor=vec4(color.rgb, 1.0);",
"	}",
"	else",
"	if(norm < 2.95 && vShape > 0.98 && (vIsContour >0.98 )){",
"	    gl_FragColor = saturationMatrix(saturation) * basis; ",
"    	gl_FragColor = vibrance(gl_FragColor, _vibrance);",
"    	gl_FragColor = shiftHue(gl_FragColor.rgb, _hue);",
"    	gl_FragColor.a = 1.0;",
"	}",
"	if(norm >=2.95 || vShape < 0.98 || vIsBorder > 0.98)",
"		gl_FragColor=basis;",
"}"
	].join( "\n" )

};
