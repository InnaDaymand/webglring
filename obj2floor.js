/*
Inna Daymand, 2018, GaazIT

objectToFloorReflexShader

input texture: postprocessing.rtBlurringFloorTexture.texture
			   postprocessing.rtContourTexture.texture 
*/

objectToFloorReflexShader = {

	uniforms: {
    "resolution": {
      "name": "resolution",
      "type": "v2",
      "glslType": "vec2",
      "description": ""
    },
    "lightPosition": {
      "name": "lightPosition",
      "type": "v3",
      "glslType": "vec3",
      "description": ""
    },
	"uObjectToFloor" : {
		"name" :"uObjectToFloor",
		"value" : null
	},
	"uBasis" : {
		"name" :"uBasis",
		"value" : null
	},
	},

	vertexShader: [
"precision highp float;",
"precision highp int;",

"uniform mat4 modelMatrix;", //= object.MatrixWorld
"uniform mat4 modelViewMatrix;", // = camera.matrixWorldInverse*object.matrixWorld
"uniform mat4 projectionMatrix;", // = camera.projectionMatrix
"uniform mat4 viewMatrix;", // = camera.matrixWorldInverse
"uniform mat3 normalMatrix;", // = inverse transpose of modelViewMatrix

"attribute vec3 position;",
"attribute vec3 normal;",
"attribute float shape;",

"uniform vec3 lightPosition;",
"uniform vec2 resolution;",


"varying vec3 vWorldPosition;",
"varying vec3 vWorldNormal;",
"varying vec2 vUv;",
"varying float vShape;",

"void main() {",

    // This sets the position of the vertex in 3d space. The correct math is
    // provided below to take into account camera and object data.
"	vec4 mv_position=modelViewMatrix * vec4( position, 1.0 );",
"	vec4 worldPosition = modelMatrix * vec4( position, 1.0 );",
"	vWorldPosition =worldPosition.xyz; ",
"	vWorldNormal =  mat3( modelMatrix[0].xyz, modelMatrix[1].xyz, modelMatrix[2].xyz ) * normal ;",
"   gl_Position = projectionMatrix * mv_position;",
// 	getting UV coordinates of position in input texture
"	float x1 = ((gl_Position.x/gl_Position.w)+1.0)*resolution.x/2.0;",
"	float y1 = ((gl_Position.y/gl_Position.w)+1.0)*resolution.y/2.0;",
"	vUv.x = (x1)/resolution.x;",
"	vUv.y = (y1)/resolution.y;",
//----------------------------------------------------------
"	vShape=shape;",
"}"

	].join( "\n" ),

	fragmentShader: [
	"precision highp float;",

// The texture result of rendering step 2.

	"uniform sampler2D uBasis;",
	"uniform sampler2D uObjectToFloor;",
	"uniform vec2 resolution;",
	"uniform vec3 lightPosition;",
	"uniform vec3 cameraPosition;", // = camera position in world space

	"varying vec2 vUv;",
	"varying vec3 vWorldPosition;",
	"varying vec3 vWorldNormal;",
	"varying float vShape;",
	

"float normColor(vec3 c){",
"	return(c.r+c.g+c.b);",
"}",
"",
"",
"",

"void main() {",


"	vec4 obj2floor=(texture2D(uObjectToFloor, vUv));",
"	vec4 basis=(texture2D(uBasis, vUv));",
"	vec4 result=vec4(0.0);",
"	if(vShape <= 0.98){",
"		float ncBasis=normColor(basis.rgb);",
"		float ncObj2floor=normColor(obj2floor.rgb);",
"		if(ncObj2floor > ncBasis)",
"			result=vec4(1.03*(mix(basis, obj2floor, 0.3).rgb), 1.0);",
"		else{",
"			result=vec4(1.2*(mix(basis, obj2floor, 1.0).rgb), 1.0);",
"		}",
"		gl_FragColor=result;",
"	}",
"	if(vShape > 0.98)",
"		gl_FragColor = vec4(basis.rgb, 1.0);",
"}"
	].join( "\n" )

};
