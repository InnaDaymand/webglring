/*
Inna Daymand, 2018, GaazIT

Gold Material Shader

using model Cook–Torrance for BRDF

attributes for model

shininess
diffuse color
specular color
ambient color
index of refraction (IOR)

for getting of gold color I use three vectors of color in composition of models

input texture: postprocessing.rtShadowMaterial.texture

*/

GoldMaterialShader = {

	uniforms: {
    "lightPosition": {
      "name": "lightPosition",
      "type": "v3",
      "glslType": "vec3",
      "description": ""
    },
    "lightColor": {
      "name": "lightColor",
      "type": "c",
      "glslType": "vec3",
      "description": ""
    },
	"redBalance": {
	  "name": "redBalance",
	  "value": "0.0",
	},
    "resolution": {
      "name": "resolution",
      "type": "v2",
      "glslType": "vec2",
      "description": ""
    },
	"uShadow": {
	  "name": "uShadow",
	  "value": null,
	},
 	},

	vertexShader: [
"precision highp float;",
"precision highp int;",

"uniform mat4 modelMatrix;", //= object.MatrixWorld
"uniform mat4 modelViewMatrix;", // = camera.matrixWorldInverse*object.matrixWorld
"uniform mat4 projectionMatrix;", // = camera.projectionMatrix
"uniform mat4 viewMatrix;", // = camera.matrixWorldInverse
"uniform mat3 normalMatrix;", // = inverse transpose of modelViewMatrix
"uniform vec3 cameraPosition;", // = camera position in world space

"attribute vec3 position;",
"attribute vec3 normal;",
"attribute float isContour;",
"attribute float randomValue;",

	"uniform vec3 lightPosition;",
	"uniform vec2 resolution;",

"varying vec3 vPositionWorld;",
"varying vec3 vPositionWorldLight;",
"varying vec3 vPositionWorldView;",
"varying vec3 vNormal;",
"varying float vIsContour;",
"varying float vRandomValue;",
"varying vec2 vUv;",

"void main() {",

"	vNormal =  mat3( modelMatrix[0].xyz, modelMatrix[1].xyz, modelMatrix[2].xyz ) * normal ;",
"	vIsContour = isContour;",     
"	vRandomValue=randomValue;",

    // This sets the position of the vertex in 3d space. The correct math is
    // provided below to take into account camera and object data.
"	 vec4 mv_position=modelViewMatrix * vec4( position, 1.0 );",
"	 vPositionWorld=vec3(modelMatrix * vec4( position, 1.0 ));",
"	 vPositionWorldLight=lightPosition - vPositionWorld;",
"	 vPositionWorldView=cameraPosition - vPositionWorld;",

"    gl_Position = projectionMatrix * mv_position;",
"	float x1 = ((gl_Position.x/gl_Position.w)+1.0)*resolution.x/2.0;",
"	float y1 = ((gl_Position.y/gl_Position.w)+1.0)*resolution.y/2.0;",
"	vUv.x = (x1)/resolution.x;",
"	vUv.y = (y1)/resolution.y;",

"}"

	].join( "\n" ),

	fragmentShader: [
	"precision highp float;",

	"uniform vec3 cameraPosition;",

// Custom uniforms
	"uniform vec3 lightColor;",
	"uniform vec3 lightPosition;",
	"uniform float redBalance;",


	"varying vec3 vPositionWorldLight;",
	"varying vec3 vPositionWorldView;",
	"varying vec3 vPositionWorld;",
	"varying vec3 vNormal;",
	"varying float vIsContour;",
	"varying float vRandomValue;",
	"varying vec2 vUv;",
	"uniform sampler2D uShadow;",
	
// constants	
	"#define pi 3.14159275358979", 
	"#define EPS 4e-4",

//---------------------------------------------------------
	"struct Material",
	"{",
		"vec3 color;		",// color of material
		"vec3 ambient;		",// ambient coefficient
		"vec3 diffuse;		",// diffuse coefficient
		"vec3 specular;		",// specular coefficient
		"vec3 ior;			",// ior for r g b F0
		"float shininess;	",// shininess
		"float emissiveIntensity;	",// emissiveIntensity
	"};",
//---------------------------------------------------------
// examples of material 115.,110.,100. //135.,125.,105.//115.,110.,105.//93.,95.,95.vec3(73.,75.,75.)/255.
// gold 208.,189.,157.
	"const Material gold = Material(vec3(91.,93.,95.)/255., vec3(0.24725, 0.1995, 0.0745), vec3(0.75164, 0.60648, 0.22648), vec3(0.628281, 0.555802, 0.366065), vec3(0.989, 0.876, 0.399), 1.7, 0.8);",
	
//----------------------------------------------------------
"float normColor(vec3 color){",
"	return (color.r+color.g+color.b);",
"}",
//----------------------------------------------------------
"	vec3 colorRedBalance(vec3 color){",
"		color.b=color.b - redBalance;",
"		return color;",
"",
"	}",

"float rand(vec2 v){",
"	return (vRandomValue*(v.y - v.x) + v.x);",
"",
"",
"}",
//color balance
"	vec3 colorBalance(vec3 color){",
"		if(color.g < color.b)",
"			color.b=color.g;",
"		float maxColorNorm=2.98;",
"		float kNorm=1.0/255.0;",
"		float min_drg=10.0;",
"		float max_drg=30.0;",
"		if(normColor(color) >=maxColorNorm)",
"			return color;",
"		float drg=color.r-color.g;",
"		if(drg < min_drg*kNorm){",
"			color.g=color.g-rand(vec2(min_drg, min_drg+5.0))*kNorm;",
"		}",
"		if(drg >max_drg*kNorm){",
"			drg=drg-(max_drg - 5.0)*kNorm;",
"			color.r=color.r-drg;",
"		}",
"		float min_dgb=30.0;",
"		float max_dgb=50.0;",
"		float limit_dgb=100.0;",
"		float dgb=color.g-color.b;",
"		if(dgb >min_dgb*kNorm && dgb < max_dgb*kNorm){",
"			dgb=dgb-rand(vec2(min_dgb - 5.0, min_dgb))*kNorm;",
"			color.b=color.b +dgb;",
"		}",
"		if(dgb >=max_dgb*kNorm && dgb < limit_dgb*kNorm){",
"			color.b=color.b +dgb*0.8;",
"		}",
"		if(dgb >= limit_dgb*kNorm){",
"			color.b=color.g -  rand(vec2(min_drg, min_dgb))*kNorm;",
"		}",
"		if(dgb < min_drg*kNorm && dgb > 0.0){",
"			color.b=color.b - rand(vec2(min_drg, min_drg+5.0))*kNorm;",
"		}",
"		if(dgb <= 0.0){",
"			color.b=color.g - rand(vec2(min_dgb/2.0, min_dgb/2.0+5.0))*kNorm;",
"		}",
"		color=colorRedBalance(color);",
"		return color;",
"	}",

//---------------------------------------------------------------------------------------
"void main() {",

    // Calculate the real position of this pixel in 3d space, taking into account
    // the rotation and scale of the model. It's a useful formula for some effects.
    // This could also be done in the vertex shader
"    vec3 vPositionW = normalize(vPositionWorld);",

"    vec3 vNormalW = normalize(vNormal);",
// Light
"	 vec3 viewDirectionW=normalize(vPositionWorldView);",
"	 vec3 lightPositionW=normalize(vPositionWorldLight);",
"	 float beckmann=0.0;",
"	 float halfCosAngle=0.; float specAngle=0.; float lightCosAngle=0.;",
"	 vec3 color=vec3(0.);",
// model Cook–Torrance for specular light
// m - m is the rms slope of the surface microfacets (the roughness of the material), we believe that m from interval (0, 1)
// Beckmann distribution
" 	 float lambertian = max(dot(vNormalW, lightPositionW), 0.00001);",
"	 vec3 halfDir=normalize(lightPositionW+viewDirectionW);",
"	 halfCosAngle=max(dot(halfDir, vNormalW), 0.00001);",
"	 specAngle=acos(halfCosAngle);",
"	 float m=0.0;",
"	 float intensity=1.0;",
"	 if (vIsContour >= 0.95){",
"		intensity = 3.5;",
"	 	m=0.3;",
"	 }",
"	 else{",
"	 	intensity=1.95;",
"	 	m=0.27;",
"	 }",
"	 beckmann=(exp(-pow((tan(specAngle)/m), 2.)))/(pi*m*m*pow(halfCosAngle,4.));",
// model Cook–Torrance for specular light
// Shielding and Shading, microfacets
"	 float Gm=(2.*(dot(vNormalW, halfDir))*(dot(vNormalW, lightPositionW)))/dot(halfDir, lightPositionW);",
"	 float Gs=(2.*(dot(vNormalW, halfDir))*(dot(vNormalW, viewDirectionW)))/dot(halfDir, lightPositionW);", 
"	 float G=min(1., min(Gm, Gs));",
// model Cook–Torrance for light in point
// Linear combination for color
"	 float const_sun=.5;",
"	 float eta_red=(1. + sqrt(gold.ior.r)/(1.-sqrt(gold.ior.r)));",
"	 float g_red=sqrt(eta_red*eta_red + lambertian*lambertian - 1.);",
"	 float fresnel_red2=(1. + pow(((lambertian*(g_red+lambertian)-1.))/((lambertian*(g_red-lambertian)-1.)),2.));",
"	 float fresnel_red1=0.5*pow((g_red - lambertian),2.)/pow((g_red + lambertian),2.);",
"	 float fresnel_red=fresnel_red1*fresnel_red2;",
"	 float Ired=(gold.color.r)*(0.3*intensity*gold.ambient.r*gold.ior.r + intensity*gold.diffuse.r*const_sun*gold.ior.r*lambertian+ intensity*gold.specular.r*const_sun*fresnel_red*beckmann*G/dot(viewDirectionW, vNormalW));",
"	 float eta_green=(1. + sqrt(gold.ior.g)/(1.-sqrt(gold.ior.g)));",
"	 float g_green=sqrt(eta_green*eta_green + lambertian*lambertian - 1.);",
"	 float fresnel_green2=(1. + pow(((lambertian*(g_green+lambertian)-1.))/((lambertian*(g_green-lambertian)-1.)),2.));",
"	 float fresnel_green1=0.5*pow((g_green - lambertian),2.)/pow((g_green + lambertian),2.);",
"	 float fresnel_green=fresnel_green1*fresnel_green2;",
"	 float Igreen=(gold.color.g)*(0.3*intensity*gold.ambient.g*gold.ior.g + intensity*gold.diffuse.g*const_sun*gold.ior.g*lambertian+ intensity*gold.specular.g*const_sun*fresnel_green*beckmann*G/dot(viewDirectionW, vNormalW));",
"	 float eta_blue=(1. + sqrt(gold.ior.b)/(1.-sqrt(gold.ior.b)));",
"	 float g_blue=sqrt(eta_blue*eta_blue + lambertian*lambertian - 1.);",
"	 float fresnel_blue2=(1. + pow(((lambertian*(g_blue+lambertian)-1.))/((lambertian*(g_blue-lambertian)-1.)),2.));",
"	 float fresnel_blue1=0.5*pow((g_blue - lambertian),2.)/pow((g_blue + lambertian),2.);",
"	 float fresnel_blue=fresnel_blue1*fresnel_blue2;",
"	 float Iblue=(gold.color.b)*(0.3*intensity*gold.ambient.b*gold.ior.b + intensity*gold.diffuse.b*const_sun*gold.ior.b*lambertian+ intensity*gold.specular.b*const_sun*fresnel_blue*beckmann*G/dot(viewDirectionW, vNormalW));",
"	 vec3 CT=vec3(Ired, Igreen, Iblue);",
"	 CT*=gold.shininess;",
"	 vec3 colf=vec3( gold.ambient*intensity +CT);",
//"	 float norm=colf.r + colf.g + colf.b;",
//"	 if(norm >= 3.0)",
//"		colf=vec3(1.0, 1.0, 1.0);",
//"	 else{",
//"	 	colf=mix(colf, colorBalance(colf), 0.95);",
//"	 	colf=mix(colf, vec3(250./255., 215./255., 0.), 0.1);",
//"	 	colf=mix(colf, vec3(150./255., 120./255., 80./255.), 0.15);",
"		colf=colorBalance(colf);",
"	 	vec3 shadow=(texture2D(uShadow, vUv)).rgb;",
" 		colf=mix(colf, shadow, 0.31);",
//"	 }",
"	 gl_FragColor=vec4(colf,1.0);", 
"}"
	].join( "\n" )

};
