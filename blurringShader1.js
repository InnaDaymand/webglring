/*
blurring

use as input reflex textures that is result of rendering of FinalReflexShader

*/

blurringShader1 = {

	uniforms: {
    "resolution": {
      "name": "resolution",
      "type": "v2",
      "glslType": "vec2",
      "description": ""
    },
    "lightPosition": {
      "name": "lightPosition",
      "type": "v3",
      "glslType": "vec3",
      "description": ""
    },
	"uBasis" : {
		"name" :"uBasis",
		"value" : null
	},
	},
	vertexShader: [
"precision highp float;",
"precision highp int;",

"uniform mat4 modelMatrix;", //= object.MatrixWorld
"uniform mat4 modelViewMatrix;", // = camera.matrixWorldInverse*object.matrixWorld
"uniform mat4 projectionMatrix;", // = camera.projectionMatrix
"uniform mat4 viewMatrix;", // = camera.matrixWorldInverse
"uniform mat3 normalMatrix;", // = inverse transpose of modelViewMatrix
"uniform vec3 cameraPosition;", // = camera position in world space

"attribute vec3 position;",
"attribute vec3 normal;",
"attribute float shape;",
"attribute vec2 uv;",
"attribute float isContour;",
"attribute float isBorder;",
"attribute float isBorder1;",

"uniform vec3 lightPosition;",
"uniform vec2 resolution;",


"varying vec3 vWorldPosition;",
"varying vec3 vWorldNormal;",
"varying vec2 vUv;",
"varying float vShape;",
"varying vec2 vtex_coord;",
"varying float vIsContour;",
"varying float vIsBorder;",
"varying float vIsBorder1;",

"void main() {",

    // This sets the position of the vertex in 3d space. The correct math is
    // provided below to take into account camera and object data.
"	vec4 mv_position=modelViewMatrix * vec4( position, 1.0 );",
"	vec4 worldPosition = modelMatrix * vec4( position, 1.0 );",
"	vWorldPosition =worldPosition.xyz; ",

"	vWorldNormal = normalize( mat3( modelMatrix[0].xyz, modelMatrix[1].xyz, modelMatrix[2].xyz ) * normal );",

"	 vIsContour = isContour;",     

"   gl_Position = projectionMatrix * mv_position;",
"	float x1 = ((gl_Position.x/gl_Position.w)+1.0)*resolution.x/2.0;",
"	float y1 = ((gl_Position.y/gl_Position.w)+1.0)*resolution.y/2.0;",
"	vUv.x = (x1)/resolution.x;",
"	vUv.y = (y1)/resolution.y;",
"	vShape=shape;",
"	vtex_coord=uv;",
"}"

	].join( "\n" ),

	fragmentShader: [
"precision highp float;",

	
"uniform sampler2D uBasis;",
"uniform vec2 resolution;",


"varying vec3 vWorldPosition;",
"varying vec3 vWorldNormal;",
"varying vec2 vUv;",
"varying float vShape;",
"varying vec2 vtex_coord;",
"varying float vIsContour;",
"varying float vIsBorder;",
"varying float vIsBorder1;",

"    mat3 normal=mat3(",
"      0.0, 0.0, 0.0,",
"      0.0, 1.0, 0.0,",
"      0.0, 0.0, 0.0",
"    );",

"   float gaussianBlur[25];",

"	float setGaussianBlur(){",
"       gaussianBlur[0]=0.045;  gaussianBlur[1]=2.0;  gaussianBlur[2]=4.0;  gaussianBlur[3]=2.0;  gaussianBlur[4]=0.045;",
"       gaussianBlur[5]=0.122;  gaussianBlur[6]=3.0;  gaussianBlur[7]=6.0;  gaussianBlur[8]=3.0;  gaussianBlur[9]=0.122;",
"       gaussianBlur[10]=2.0;   gaussianBlur[11]=4.0; gaussianBlur[12]=8.0; gaussianBlur[13]=4.0; gaussianBlur[14]=2.0; ",
"       gaussianBlur[15]=0.122; gaussianBlur[16]=3.0; gaussianBlur[17]=6.0; gaussianBlur[18]=3.0; gaussianBlur[19]=0.122;",
"       gaussianBlur[20]=0.045; gaussianBlur[21]=2.0; gaussianBlur[22]=4.0; gaussianBlur[23]=2.0; gaussianBlur[24]=0.045;",
"		return 0.0;",
"	}",

"    mat3 emboss = mat3(",
"       -2.0, -1.0,  0.0,",
"       -1.0,  1.0,  1.0,",
"        0.0,  1.0,  2.0",
"   );",
"    mat3 sharpen = mat3(",
"       -1.0, -1.0, -1.0,",
"       -1.0, 16.0, -1.0,",
"       -1.0, -1.0, -1.0",
"    );",

"float hash(float n)",
"{",
"    return fract(sin(n)*10753.5453123);",
"}",

"float noise(vec4 point)",
"{",
"    vec3 p = floor(point.xyz);",
"    vec3 f = fract(point.xyz);",
"    f = f*f*(3.0-2.0*f);",
	 	
"    float n = p.x + p.y*57.0 + 113.0*p.z;",
"    return mix(mix(mix(hash(n+  0.0), hash(n+  1.0),f.x),",
"                   mix(hash(n+157.0), hash(n+158.0),f.x),f.y),",
"               mix(mix(hash(n+113.0), hash(n+114.0),f.x),",
"                   mix(hash(n+270.0), hash(n+271.0),f.x),f.y),f.z);",
"}",
"",
"float normColor(vec3 color){",
"	return (color.r+color.g+color.b);",
"}",

"float weight(float kernel[25]){",
"	float sum=0.0;",
"	for(int i=0; i< 25; i++){",
"			sum+=kernel[i];",
"	}",
"	return sum <=0.0 ? 1.0:sum;",
"}",

"void main() {",
	
"	vec2 onePixel = vec2(1.0, 1.0) / (resolution);",
"	if(vShape > 0.99 && vShape < 1.98 && (vIsContour < 0.98 || vIsBorder <0.98 || vIsBorder1 < 0.98)){",
"		setGaussianBlur();",
"   	vec4 colorSum1 =",
"     	texture2D(uBasis, vUv + onePixel * vec2(-2, -2)) * gaussianBlur[0] +",
"     	texture2D(uBasis, vUv + onePixel * vec2( -1, -2)) * gaussianBlur[1] +",
"     	texture2D(uBasis, vUv + onePixel * vec2( 0, -2)) * gaussianBlur[2] +",
"     	texture2D(uBasis, vUv + onePixel * vec2(1,  -2)) * gaussianBlur[3] +",
"     	texture2D(uBasis, vUv + onePixel * vec2( 2,  -2)) * gaussianBlur[4] +",
"     	texture2D(uBasis, vUv + onePixel * vec2( -2,  -1)) * gaussianBlur[5] +",
"     	texture2D(uBasis, vUv + onePixel * vec2(-1,  -1)) * gaussianBlur[6] +",
"     	texture2D(uBasis, vUv + onePixel * vec2( 0,  -1)) * gaussianBlur[7] +",
"     	texture2D(uBasis, vUv + onePixel * vec2( 1,  -1)) * gaussianBlur[8] +",
"     	texture2D(uBasis, vUv + onePixel * vec2(2, -1)) * gaussianBlur[9] +",
"     	texture2D(uBasis, vUv + onePixel * vec2( -2, 0)) * gaussianBlur[10] +",
"     	texture2D(uBasis, vUv + onePixel * vec2( -1, 0)) * gaussianBlur[11] +",
"     	texture2D(uBasis, vUv + onePixel * vec2(0,  0)) * gaussianBlur[12] +",
"     	texture2D(uBasis, vUv + onePixel * vec2( 1,  0)) * gaussianBlur[13] +",
"     	texture2D(uBasis, vUv + onePixel * vec2( 2,  0)) * gaussianBlur[14] +",
"     	texture2D(uBasis, vUv + onePixel * vec2(-2,  1)) * gaussianBlur[15] +",
"     	texture2D(uBasis, vUv + onePixel * vec2( -1,  1)) * gaussianBlur[16] +",
"     	texture2D(uBasis, vUv + onePixel * vec2( 0,  1)) * gaussianBlur[17] +",
"     	texture2D(uBasis, vUv + onePixel * vec2( 1,  1)) * gaussianBlur[18] +",
"     	texture2D(uBasis, vUv + onePixel * vec2( 2,  1)) * gaussianBlur[19] +",
"     	texture2D(uBasis, vUv + onePixel * vec2(-2,  2)) * gaussianBlur[20] +",
"     	texture2D(uBasis, vUv + onePixel * vec2( -1,  2)) * gaussianBlur[21] +",
"     	texture2D(uBasis, vUv + onePixel * vec2( 0,  2)) * gaussianBlur[22] +",
"     	texture2D(uBasis, vUv + onePixel * vec2( 1,  2)) * gaussianBlur[23] +",
"     	texture2D(uBasis, vUv + onePixel * vec2( 2,  2)) * gaussianBlur[24] ;",
"		float w=weight(gaussianBlur);",
"		colorSum1=(colorSum1/w);",
"		float noisef=noise(colorSum1);",
"		colorSum1.xyz+= mix(-0.10, 0.10, noisef); ",
"		float normColor= normColor(colorSum1.xyz);",
"		if(normColor <= 2.0625){",
"			colorSum1.xyz=colorSum1.xyz*0.85;",
"		}",
"		gl_FragColor=vec4(colorSum1.xyz, 1.0);",
"	}",
"	else",
"		gl_FragColor=texture2D(uBasis, vUv);",
"}"
	].join( "\n" )

};

