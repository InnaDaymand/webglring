/*
ReflexShader

use as input cube texture that is result of rendering of cubeCamera

*/

CausticsShader = {

	uniforms: {
    "resolution": {
      "name": "resolution",
      "type": "v2",
      "glslType": "vec2",
      "description": ""
    },
    "lightPosition": {
      "name": "lightPosition",
      "type": "v3",
      "glslType": "vec3",
      "description": ""
    },
	"u_texture" : {
		"name" :"u_texture",
		"value" : null
	},
	"ior": { 
		"name" : "ior",
		"type": "v3",
		"glslType" : "vec3",
		"value": "", 
	},
	"fresnelBias": { 
		"value": "0.01" 
	},
	"fresnelPower": { 
		"value": "2.0" 
	},
	"fresnelScale": { 
		"value": ".5" 
	},
	},

	vertexShader: [
"precision highp float;",
"precision highp int;",

"uniform mat4 modelMatrix;", //= object.MatrixWorld
"uniform mat4 modelViewMatrix;", // = camera.matrixWorldInverse*object.matrixWorld
"uniform mat4 projectionMatrix;", // = camera.projectionMatrix
"uniform mat4 viewMatrix;", // = camera.matrixWorldInverse
"uniform mat3 normalMatrix;", // = inverse transpose of modelViewMatrix
"uniform vec3 cameraPosition;", // = camera position in world space

"attribute vec3 position;",
"attribute vec3 normal;",
"attribute vec2 uv;",
"attribute float shape;",
"attribute float isContour;",
"attribute float isBorder;",
"attribute float randomValue;",
                                             

"uniform vec2 resolution;",
"uniform vec3 lightPosition;",
"uniform vec3 ior;",
"uniform float fresnelBias;",
"uniform float fresnelPower;",
"uniform float fresnelScale;",

"varying vec3 vReflect;",
"varying vec3 vRefract[3];",
"varying float vReflectionFactor;",
"varying vec3 vWorldPosition;",
"varying float vShape;",
"varying float vIsContour;",
"varying float vIsBorder;",
"varying float vRandomValue;",

"void main() {",

"	 vec3 lNormal=normalize(mat3(normalMatrix)*normal);",
    // This sets the position of the vertex in 3d space. The correct math is
    // provided below to take into account camera and object data.
"	 vec4 mv_position=modelViewMatrix * vec4( position, 1.0 );",
"	vec4 worldPosition = modelMatrix * vec4( position, 1.0 );",
"	vWorldPosition =worldPosition.xyz; ",

"	vec3 worldNormal = normalize( mat3( modelMatrix[0].xyz, modelMatrix[1].xyz, modelMatrix[2].xyz ) * normal );",

"	vec3 I = worldPosition.xyz - cameraPosition;",
"	vec3 L = worldPosition.xyz - lightPosition;",
"	vec3 D = I;",
"	vReflect=vec3(0.0);",
"	if(shape < 0.98){",
"		vReflect = reflect( D, worldNormal );",
"		vReflectionFactor = fresnelBias + fresnelScale * pow( 1.0 + dot(  D , worldNormal ), fresnelPower );",
"		vRefract[0] = vec3(0.0);",
"		vRefract[1] = vec3(0.0);",
"		vRefract[2] = vec3(0.0);",
"	}",
"	if(shape > 0.98){",
"		vRefract[0] = refract( D , worldNormal, ior.r );",
"		vRefract[1] = refract( D , worldNormal, ior.g );",
"		vRefract[2] = refract( D , worldNormal, ior.b );",
"	}",
"   gl_Position = projectionMatrix * mv_position;",
"	vShape=shape;",
"	vIsContour=isContour;",
"	vIsBorder=isBorder;",
"	vRandomValue=randomValue;",

"}"

	].join( "\n" ),

	fragmentShader: [
	"precision highp float;",


// The texture result of rendering step 1.

	"uniform samplerCube u_texture;",

	"varying vec3 vReflect;",
	"varying vec3 vRefract[3];",
	"varying float vReflectionFactor;",
	"varying vec3 vWorldPosition;",
	
	"varying float vShape;",
	"varying float vIsContour;",
	"varying float vIsBorder;",
	"varying float vRandomValue;",
	
// constants	
	"#define pi 3.14159275358979", 
	"#define EPS 4e-4",

"void main() {",
"	vec4 reflectedColor=vec4(1.0);",
"	if(vShape < 0.98){",	
"		reflectedColor= textureCube(u_texture, vReflect);",
"	}",
"	vec4 refractedColor = vec4( 1.0 );",
"	if(vShape > 0.98){",
"		refractedColor.r = textureCube( u_texture, vec3( vRefract[0].x, vRefract[0].yz ) ).r;",
"		refractedColor.g = textureCube( u_texture, vec3( vRefract[1].x, vRefract[1].yz ) ).g;",
"		refractedColor.b = textureCube( u_texture, vec3( vRefract[2].x, vRefract[2].yz ) ).b;",
"	}",

//"	gl_FragColor = mix( refractedColor, reflectedColor, (1.0 - clamp( vReflectionFactor, 0.0, 1.0 )) );",
"	gl_FragColor=reflectedColor;",

"}"
	].join( "\n" )

};
