/*
webgl utils


*/

  "use strict";
// shaders and program  
var vertexShader;
var fragmentShader;
var program;

// location of attributes
var positionAttributeLocation;
var textcoordAttributeLocation;
var colorAttributeLocation;

// location of uniforms
var modelViewMatrixLocation;
var projectionMatrixLocation;
var color_BottomLocation;
var color_BottomLeftLocation;
var color_BottomRightLocation;
var color_TopLocation;
var color_TopLeftLocation;
var color_TopRightLocation;

// buffers for attributes`
var positionBuffer;
var textcoordBuffer;
var colorBuffer;
  
function createShader(gl, type, source) {
	var shader = gl.createShader(type);
	gl.shaderSource(shader, source);
	gl.compileShader(shader);
	var success = gl.getShaderParameter(shader, gl.COMPILE_STATUS);
	if (success) {
		return shader;
	}
	
	console.log(gl.getShaderInfoLog(shader));

	gl.deleteShader(shader);
}

function createProgram(gl, vertexShader, fragmentShader) {
	var program = gl.createProgram();
	gl.attachShader(program, vertexShader);
	gl.attachShader(program, fragmentShader);
	gl.linkProgram(program);
	var success = gl.getProgramParameter(program, gl.LINK_STATUS);
	if (success) {
		return program;
	}

	console.log(gl.getProgramInfoLog(program));
	gl.deleteProgram(program);
}

function setSimpleShader(gl, shader){
	vertexShader = createShader(gl, gl.VERTEX_SHADER, shader.vertexShader);
	fragmentShader = createShader(gl, gl.FRAGMENT_SHADER, shader.fragmentShader);

	// Link the two shaders into a program
	program = createProgram(gl, vertexShader, fragmentShader);

	// look up where the vertex data needs to go.
	positionAttributeLocation = gl.getAttribLocation(program, "a_position");
	textcoordAttributeLocation = gl.getAttribLocation(program, "a_textcoord");
	colorAttributeLocation = gl.getAttribLocation(program, "a_color");
	
	// add uniforms
	modelViewMatrixLocation = gl.getUniformLocation(program, "modelViewMatrix");
	projectionMatrixLocation = gl.getUniformLocation(program, "projectionMatrix");
	color_BottomLocation= gl.getUniformLocation (program,  "ucolor_bottom");    
	color_BottomLeftLocation= gl.getUniformLocation (program,  "ucolor_bottomleft");    
	color_BottomRightLocation= gl.getUniformLocation (program,  "ucolor_bottomright");    
	color_TopLocation= gl.getUniformLocation (program,  "ucolor_top");    
	color_TopLeftLocation= gl.getUniformLocation (program,  "ucolor_topleft");    
	color_TopRightLocation= gl.getUniformLocation (program,  "ucolor_topright");    
	

		// Tell WebGL how to convert from clip space to pixels
	gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);

    // Clear the canvas.
   gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    // Turn on culling. By default backfacing triangles
    // will be culled.
    gl.enable(gl.CULL_FACE);

    // Enable the depth buffer
  gl.enable(gl.DEPTH_TEST);

	// Tell it to use our program (pair of shaders)
	gl.useProgram(program);

	// Create a buffer and put three 2d clip space points in it
	positionBuffer = gl.createBuffer();
	textcoordBuffer = gl.createBuffer();


	colorBuffer = gl.createBuffer();
}

function runSimpleShader(gl, data, mode, count_in){
    gl.uniformMatrix4fv(modelViewMatrixLocation, false, data.modelViewMatrix.elements);
    gl.uniformMatrix4fv(projectionMatrixLocation, false, data.projectionMatrix.elements);
	gl.uniform4f (color_BottomLocation, data.colorBottom.r, data.colorBottom.g, data.colorBottom.b, 1);    
	gl.uniform4f (color_BottomLeftLocation, data.colorBottomLeft.r, data.colorBottomLeft.g, data.colorBottomLeft.b, 1);    
	gl.uniform4f (color_BottomRightLocation, data.colorBottomRight.r, data.colorBottomRight.g, data.colorBottomRight.b, 1);    
	gl.uniform4f (color_TopLocation, data.colorTop.r, data.colorTop.g, data.colorTop.b, 1);    
	gl.uniform4f (color_TopLeftLocation, data.colorTopLeft.r, data.colorTopLeft.g, data.colorTopLeft.b, 1);    
	gl.uniform4f (color_TopRightLocation, data.colorTopRight.r, data.colorTopRight.g, data.colorTopRight.b, 1);    

	// Bind it to ARRAY_BUFFER (think of it as ARRAY_BUFFER = positionBuffer)
	gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
	gl.enableVertexAttribArray(positionAttributeLocation);
	// Tell the attribute how to get data out of positionBuffer (ARRAY_BUFFER)
	var size = count_in;          // 2 components per iteration
	var type = gl.FLOAT;   // the data is 32bit floats
	var normalize = false; // don't normalize the data
	var stride = 0;        // 0 = move forward size * sizeof(type) each iteration to get the next position
	var offset = 0;        // start at the beginning of the buffer
	gl.vertexAttribPointer(
	  positionAttributeLocation, size, type, normalize, stride, offset);

	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(data.positions), gl.STATIC_DRAW);
	
	// Bind it to ARRAY_BUFFER (think of it as ARRAY_BUFFER = positionBuffer)
	gl.bindBuffer(gl.ARRAY_BUFFER, textcoordBuffer);
	gl.enableVertexAttribArray(textcoordAttributeLocation);
	// Tell the attribute how to get data out of positionBuffer (ARRAY_BUFFER)
	var size = 2;          // 2 components per iteration
	var type = gl.FLOAT;   // the data is 32bit floats
	var normalize = false; // don't normalize the data
	var stride = 0;        // 0 = move forward size * sizeof(type) each iteration to get the next position
	var offset = 0;        // start at the beginning of the buffer
	gl.vertexAttribPointer(
	  textcoordAttributeLocation, size, type, normalize, stride, offset);

	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(data.texcoords), gl.STATIC_DRAW);
	
	gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);
	// Turn on the attribute
    gl.enableVertexAttribArray(colorAttributeLocation);

	size=4;
	type=gl.UNSIGNED_BYTE;
	normalize=true;
	gl.vertexAttribPointer(
		colorAttributeLocation, size, type, normalize, stride, offset);
	gl.bufferData(gl.ARRAY_BUFFER, new Uint8Array(data.colors), gl.STATIC_DRAW);

	// draw
	var primitiveType = mode;
	var offset = 0;
	var count = data.positions.length/count_in;
	gl.drawArrays(primitiveType, offset, count);

}
