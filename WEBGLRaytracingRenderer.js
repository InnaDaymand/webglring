

THREE.WEBGLRaytracingRenderer = function ( parameters ) {

	console.log( 'THREE.WEBGLRaytracingRenderer', THREE.REVISION );

	parameters = parameters || {};

	var scope = this;
	var pool = [];
	var renderering = false;

	var startX =-1;
	var endX=-1;
	var startY=-1;
	var endY=-1;
	var screenWidth, screenHeight;


	var workers = parameters.workers;
	var gl_main = parameters.gl_main;
	var gl_add = parameters.gl_add;
	var cache = parameters.cache;
	var blockSizeY = parameters.blockSizeY||1;
	var arrayInfoRenderingBlocksTmp=new Array();
	var arrayInfoRendering = new Array();
	var arrayInfoRenderingBlocks = new Array();
	var workerId = 0, sceneId = 0; 
//	var pixels=new Array();
	var pixels;
	var pixel;
	var dataPixel={};
	var yblocks;
	


	this.setBounding= function(sX, eX, sY, eY, width, height){
		startX=sX, endX=eX, startY=sY, endY=eY; screenWidth=width; screenHeight=height;
		startX = startX - Math.round((endX-startX)*0.2);
		endX = endX + Math.round((endX-startX)*0.2);
		startY=startY - Math.round((endY - startY)*0.1);
		endY=endY + Math.round((endY - startY)*0.3);
		yblocks= Math.ceil( (endY-startY) / blockSizeY );
		endY=startY + (yblocks+1)*blockSizeY;
		arrayInfoRendering.splice(0, arrayInfoRendering.length);
		arrayInfoRenderingBlocks.splice(0, arrayInfoRenderingBlocks.length);
/*		for(var y=startY ; y < endY; y++){
			for(var x=startX; x < endX; x++){
				pixel=new Uint8Array(4);
				gl_main.readPixels(x, SCREEN_HEIGHT - 1- y, 1, 1, gl_main.RGBA, gl_main.UNSIGNED_BYTE, pixel);
				dataPixel ={
					xPixel :x,
					yPixel :y,
					colorPixel:pixel
				};
				pixels.push(dataPixel);
			}
		}*/
		pixels=new Uint8Array((endX-startX)*(endY - startY +1)*4)
		gl_main.readPixels(startX, SCREEN_HEIGHT - endY, (endX-startX), ( endY - startY+1) , gl_main.RGBA, gl_main.UNSIGNED_BYTE, pixels);
		this.setWorkers(workers);
	};
	
	this.setWorkers = function( w ) {

		if(startX <0 || endX <0 || startY<0 || endY <0)
			return;
		workers = w ;

		while ( pool.length < workers ) {
			var worker = new Worker( parameters.workerPath );
			worker.id = workerId++;
			
			worker.onmessage = function( e ) {
				var data = e.data;

				if ( ! data ) return;

				if ( data.blockSizeY && sceneId == data.sceneId ) { // we match sceneId here to be sure

					arrayInfoRenderingBlocks.push(data.result);
					// completed

					console.log( 'Worker ' + this.id, data.time / 1000, ( Date.now() - reallyThen ) / 1000 + ' s' );

					if ( pool.length > workers ) {

						pool.splice( pool.indexOf( this ), 1 );
						return this.terminate();

					}

					renderNext( this );

				}

			};

			pool.push( worker );

			if ( renderering ) {

				updateSettings( worker );

				worker.postMessage( {
					scene: sceneJSON,
					camera: cameraJSON,
					sceneId: sceneId,
					texture:pixels,
				} );

				renderNext( worker );

			}

		}

		if ( ! renderering ) {

			while ( pool.length > workers ) {

				pool.pop().terminate();

			}

		}

	};

	
	this.initWorkers=function(){
		pool.forEach( updateSettings );
	}
	
	function updateSettings( worker ) {

		worker.postMessage( {

			init: 1,
			worker: worker.id,
			blockSizeY: blockSizeY,
			blockSizeX:(endX-startX)

		} );

		worker.postMessage( {

			resize: 1,
			startXA:startX,
			startYA:endY,
			width: screenWidth,
			height: screenHeight

		} );
	}

	function renderNext( worker ) {
		if ( ! arrayInfoRenderingBlocksTmp.length ) {
			completeArray[completeArray.length-1].workersCount-=1;
			renderering = false;
			console.log( 'renrer pass with worker '+worker.id+' complete ' + ( Date.now() - reallyThen ) / 1000 + ' s' );
			pool.splice( pool.indexOf( worker ), 1 );
			worker.terminate();
			if(completeArray[completeArray.length-1].workersCount == 0){
				workerId=0;
				getData();
				renderCaustics(startX, endX, startY, endY);
				drawAddCanvas();
				completeArray[completeArray.length-1].complete=1;
			}
			return scope.dispatchEvent( { type: "complete" } );
		}

		var current = arrayInfoRenderingBlocksTmp.pop();

		worker.postMessage( {
			render: true,
			sX: (startX),
			sY: (startY + current.numBlockY*blockSizeY),
			block:current,
			sceneId: sceneId
		} );

	}

	var sceneJSON, cameraJSON, reallyThen;
	var rComplete ={};
	var completeArray=new Array();
	
	this.renderAvailable = function(){
		return ((completeArray.length == 0) || (completeArray.length > 0 && completeArray[completeArray.length-1].complete == 1));
	}

	this.render = function ( scene, camera ) {

		if(completeArray.length == 0 || completeArray[completeArray.length-1].complete > 0){
			rComplete = {
				workersCount:workers,
				complete : 0,
				sceneId :++sceneId
			}
			completeArray.push(rComplete);
			renderering = true;
		}
		else {
			renderering = false;
			return;
		}

		// update scene graph

		if ( scene.autoUpdate === true ) scene.updateMatrixWorld();

		// update camera matrices

		if ( camera.parent === null ) camera.updateMatrixWorld();

		updateRenderAddCanvas(scene, camera);

		sceneJSON = scene.toJSON();
		cameraJSON = camera.toJSON();

		pool.forEach( function( worker ) {

			worker.postMessage( {
				scene: sceneJSON,
				camera: cameraJSON,
				sceneId: sceneId,
				texture:pixels,
				cache :cache,
			} );
		} );

		reallyThen = Date.now();

		arrayInfoRenderingBlocks.splice(0, arrayInfoRenderingBlocks.length);
		arrayInfoRenderingBlocksTmp.splice(0, arrayInfoRenderingBlocks.length);
		for (var i = 0; i < yblocks; i++ ) {
			var block= new THREE.InfoRenderingBlock();
			block.numBlockX=0;
			block.numBlockY=i;
			arrayInfoRenderingBlocksTmp.push(block);
		}

		pool.forEach( renderNext );

	};
	
	updateRenderAddCanvas = function (scene, camera){
	
		// set up object matrices and vertices for meshes
		camera.updateProjectionMatrix();
		
		camera.updateMatrixWorld();
		
		camera.matrixWorldInverse.getInverse( camera.matrixWorld );

		scene.traverse( function ( object ) {

			
			var _object = cache[ object.uuid ];
			
			modelViewMatrix.multiplyMatrices( camera.matrixWorldInverse, object.matrixWorld );


			_object.normalMatrix.getNormalMatrix( object.matrixWorld );
			_object.inverseMatrix.getInverse( object.matrixWorld );
			
		} );
		
	}

	function getData(){
		indexSort=0;
		for(var i=arrayInfoRenderingBlocks.length-1, k=0; i >=0 ; i--, k++){
			var iblock=arrayInfoRenderingBlocks[i];
			if(iblock.numBlockY == indexSort){
				for(var r=0; r < iblock.arrayInfoRendering.length;  r++){
					var row=iblock.arrayInfoRendering[r];
					for(var c=0; c< row.length; c++)
					arrayInfoRendering.push(row[c]);
				}
				indexSort+=1;
				arrayInfoRenderingBlocks.splice(i, 1);
				if((indexSort-1) != k){
					i=arrayInfoRenderingBlocks.length;
					k=indexSort - 1;
				}
			}
		}
	}

	function renderCaustics(renderXStart, renderXEnd, renderYStart, renderYEnd){
/*		for(var i=0; i < arrayInfoRendering.length; i++){
			var infor=arrayInfoRendering[i];
			var index1=pixels.length - 4 -  (((infor.yShape-startY))*(endX-startX)*4 + (startX - infor.xShape)*4);
			infor.colorShape.r=pixels[index1+0]/255;
			infor.colorShape.g=pixels[index1+1]/255;
			infor.colorShape.b=pixels[index1+2]/255;
		}*/
		renderYStart = arrayInfoRendering[0].yShape;
		for(var i=0; i < arrayInfoRendering.length; i++){
			var infor=arrayInfoRendering[i];
			for(var j=0; j < infor.infoLReflex.length; j++){
				var m=infor.infoLReflex[j].xCaustics - renderXStart;
				var n=infor.infoLReflex[j].yCaustics - renderYStart;
				n = n==0? 0 : n-1;
				if(m < 0 || n < 0)
					continue;
				var indexPointCaustics=n*(renderXEnd - renderXStart) + m;
				if(indexPointCaustics < arrayInfoRendering.length){
					var infoRenderCaustics=arrayInfoRendering[indexPointCaustics];
				if(/*(infoRenderCaustics.normTextureColor > 2.5 ) && */ (infoRenderCaustics.shape == 1 || infoRenderCaustics.shape == 2))
						continue;
					infoRenderCaustics.colorShape.r+=infor.infoLReflex[j].colorCausticsCT.r;
					if(infoRenderCaustics.colorShape.r > 1)
						infoRenderCaustics.colorShape.r=1;
					infoRenderCaustics.colorShape.g+=infor.infoLReflex[j].colorCausticsCT.g;
					if(infoRenderCaustics.colorShape.g > 1)
						infoRenderCaustics.colorShape.g=1;
					infoRenderCaustics.colorShape.b+=infor.infoLReflex[j].colorCausticsCT.b;
					if(infoRenderCaustics.colorShape.b > 1)
						infoRenderCaustics.colorShape.b=1;
					infoRenderCaustics.count+=1;
				}
			}
		}
		postprocessingBalance();
	}

	var Gx = [-1.0, 0.0, 1.0,
		   -2.0, 0.0, 2.0,
		   -1.0, 0.0, 1.0];
	var Gy = [-1.0, -2.0, -1.0,
		   0.0, 0.0, 0.0,
		   1.0, 2.0, 1.0];
	var vector3=new THREE.Vector3(0.1, 0.1, 0.1);
	var vectorC=new THREE.Vector3();
	var colorTMP=new THREE.Color();
	
	function applyKernel(current_index){
		var horizontal = 0.0;
		var vertical = 0.0;
		vector3.set(0.1, 0.1, 0.1);
		vectorC.set(0, 0, 0);
		for (var i = -1; i < 2; i++) {
			for (var j = -1; j < 2; j++) {
					var index = current_index + i*(endX - startX) +j;
					if(index < arrayInfoRendering.length){
					var infor=arrayInfoRendering[index];
					vectorC.set(infor.colorShape.r, infor.colorShape.g, infor.colorShape.b);
					var averagePixel = vectorC.dot(vector3);

					horizontal += averagePixel * Gx[(1+j)+(i+1)*3];
					vertical += averagePixel * Gy[(1+j)+(i+1)*3];            
				}
			}
		}
		return Math.sqrt(horizontal * horizontal + vertical * vertical);
	}
	

	function applyMyBlur(current_index, shape, radius1_in){
		colorTMP.r=0; colorTMP.g=0; colorTMP.b=0;
		var yStart=arrayInfoRendering[0].yShape;
		var radius =radius1_in;
		var infor=arrayInfoRendering[current_index];
		if(infor.yShape <=yStart+radius || infor.xShape > endX - radius)
			return;
		var indexRow=0;
		if(infor.xShape <= startX+radius){
			indexRow = (startX + radius) - infor.xShape;
		}
		else 
			indexRow = -radius;
		var color1=new THREE.Color(0, 0, 0);
		var avg=0;
		for(var i= -radius; i < radius+1; i++){
			for(var j= indexRow; j < radius+1; j++){
				var index1=current_index + i*(endX - startX) + j;
				if(index1>= arrayInfoRendering.length || index1 < 0)
					continue;
				var infor1=arrayInfoRendering[index1];
				if(infor1.shape == shape){
					color1.r+=infor1.colorShape.r;
					color1.g+=infor1.colorShape.g;
					color1.b+=infor1.colorShape.b;
					avg++;
				}
			}
		}
		if(avg > 0){
			colorTMP.r = color1.r/avg;
			colorTMP.g = color1.g/avg;
			colorTMP.b = color1.b/avg;
			if(colorTMP.r == infor.colorShape.r && colorTMP.g == infor.colorShape.g && colorTMP.b == infor.colorShape.b)
				return;
			Models.lerp(infor.colorShape, colorTMP, 0.85);
			if(infor1.colorShape.r - infor1.colorShape.g > 4)
				infor1.colorShape.g=infor1.colorShape.r - 4;
			if(infor1.colorShape.r - infor1.colorShape.b > 10)
				infor1.colorShape.b=infor1.colorShape.r - 10;
		}
	}

	function applyMyBlur1(current_index, shape, radius1_in){
		colorTMP.r=0; colorTMP.g=0; colorTMP.b=0;
		var yStart=arrayInfoRendering[0].yShape;
		var radius =radius1_in;
		var infor=arrayInfoRendering[current_index];
		if(infor.yShape <=yStart+radius || infor.xShape > endX - radius)
			return;
		var indexRow=0;
		if(infor.xShape <= startX+radius){
			indexRow = (startX + radius) - infor.xShape;
		}
		else 
			indexRow = -radius;
		var color1=new THREE.Color(0, 0, 0);
		var avg=0;
		for(var i= -radius; i < radius+1; i++){
			for(var j= indexRow; j < radius+1; j++){
				var index1=current_index + i*(endX - startX) + j;
				if(index1>= arrayInfoRendering.length || index1 < 0)
					continue;
				var infor1=arrayInfoRendering[index1];
				if(infor1.shape == shape && infor1.onContour == false){
					color1.r+=infor1.colorShape.r;
					color1.g+=infor1.colorShape.g;
					color1.b+=infor1.colorShape.b;
					avg++;
				}
			}
		}
		if(avg > 0){
			colorTMP.r = color1.r/avg;
			colorTMP.g = color1.g/avg;
			colorTMP.b = color1.b/avg;
			if(colorTMP.r == infor.colorShape.r && colorTMP.g == infor.colorShape.g && colorTMP.b == infor.colorShape.b)
				return;
			Models.lerp(infor.colorShape, colorTMP, 0.85);
		}
	}


	function colorBalance(infor){
		if(infor.normTextureColor >=2.8)
			return;
		var drg=infor.colorShape.r-infor.colorShape.g;
		if(drg < 10/255){
			infor.colorShape.g=infor.colorShape.g-THREE.Math.randInt(9, 13)/255;
		}
		if(drg >30/255){
			drg=drg-25/255;
			infor.colorShape.r=infor.colorShape.r-drg;
		}
		var dgb=infor.colorShape.g-infor.colorShape.b;
		if(dgb >30/255 && dgb < 50/255){
			dgb=dgb-25/255;
			infor.colorShape.b=infor.colorShape.b +dgb;
		}
		if(dgb >=50/255 && dgb < 100/255){
			infor.colorShape.b=infor.colorShape.b +dgb*0.8;
		}
		if(dgb >= 100/255){
			infor.colorShape.b=infor.colorShape.g -  THREE.Math.randInt(25, 40)/255;
		}
		if(dgb < 0){
			infor.colorShape.b=infor.colorShape.b - Math.abs(dgb)*2;
		}
	}

	function isPixelOnBorder(current_index){
		var infor=arrayInfoRendering[current_index];
		for(var i=-1; i<2; i++){
			for(var j=-1; j <2; j++){
				var index1=current_index + i*(endX - startX) + j;
				if(index1 < 0 || index1 >= arrayInfoRendering.length)
					continue;
				var infor1=arrayInfoRendering[index1];
				if(Math.abs(infor1.xShape - infor.xShape)>1)
					continue;
				var dist=Models.distance(infor.point, infor1.point);
				if(infor.shape == infor1.shape && dist < 0.09)
					continue;
				else{
					return true;
				}
			}
		}
		return false;
	}

	function isPixelOnNeighbourBorder(current_index){
		var infor=arrayInfoRendering[current_index];
		for(var i=-2; i<3; i++){
			for(var j=-1; j <2; j++){
				var index1=current_index + i*(endX - startX) + j;
				if(index1 < 0 || index1 >= arrayInfoRendering.length)
					continue;
				var infor1=arrayInfoRendering[index1];
				if(Math.abs(infor1.xShape - infor.xShape)>1)
					continue;
				var dist=Models.distance(infor.point, infor1.point);
				if (infor.shape == infor1.shape && dist < 0.09 && infor1.onBorder==true)
					return true;
				else
					continue;
			}
		}
		return false;
	}
	
	function postprocessingBalance(){
		var inforTMP=new THREE.InfoRendering();
		var inforTMP1=new THREE.InfoRendering();
		for(var i=0; i < arrayInfoRendering.length; i+=1){
			var infor=arrayInfoRendering[i];
			if(infor.shape == 0){
				applyMyBlur(i, infor.shape, 20);
			}
		}
		
		for(var i=0; i < arrayInfoRendering.length; i+=1){
			var infor=arrayInfoRendering[i];
			if((infor.shape ==1 && infor.onBorder == false) ){
				infor.onBorder=isPixelOnBorder(i);
			}
		}
		for(var i=0; i < arrayInfoRendering.length; i+=1){
			var infor=arrayInfoRendering[i];
			if((infor.shape ==1 && infor.onBorder == false && infor.onContour ==false) ){
				processingNormals(i, infor.shape);
			}
			if(infor.shape == 1)
				colorBalance(infor);
		}
		
		for(var i=0; i < arrayInfoRendering.length; i+=1){
			var infor=arrayInfoRendering[i];
			if((infor.shape == 1 && (infor.onContour==false) && infor.onBorder ==false  && infor.normTextureColor < 2.9)){
				applyMyFilter(i, infor.shape, 10);
			}
		}
		for(var i=0; i < arrayInfoRendering.length; i+=1){
			var infor=arrayInfoRendering[i];
			if((infor.shape == 1 && (infor.onContour==false) && infor.onBorder ==false && infor.normTextureColor < 2.4) ){
				applyMyFilterCol(i, infor.shape, 10);
			}
		}
	}
	
	
	function applyDot(current_index, index1, index2, shape){
		var infor = arrayInfoRendering[current_index];
		if(index1 >= 0 && index1 <= arrayInfoRendering.length && index2 >= 0 && index2 <= arrayInfoRendering.length){
			var infor1=arrayInfoRendering[index1];
			var infor2=arrayInfoRendering[index2];
			if(infor1.shape != shape || infor2.shape !=shape)
				return;
			var dist1=Models.distance(infor.point, infor1.point);
			var dist2=Models.distance(infor.point, infor2.point);
			if(dist1 >=0.09 || dist2 >= 0.09)
				return;
			var v13=infor1.normal;
			var v23=infor.normal;
			var v13=new THREE.Vector3(infor1.point.x - infor.point.x, infor1.point.y - infor.point.y, infor1.point.z - infor.point.z);
			var v23=new THREE.Vector3(infor2.point.x - infor.point.x, infor2.point.y - infor.point.y, infor2.point.z - infor.point.z);
			var cross=Models.cross(v13, v23);
			var val = Models.length(cross);
			if(Math.abs(val) > 0.35){
				var infpoint=new THREE.InfoInflectionPoint();
				infpoint.xInfPoint=infor1.xShape;
				infpoint.yInfPoint=infor1.yShape;
				infpoint.sinAngle = val;
				applyMyFilterContour(current_index, shape, 10, infpoint);
				infor.onContour=true;
				infor1.onContour=true;
				infor2.onContour=true;
				infor.infoInflectionPoints.push(infpoint);
			}
		}
	}
	
	function processingNormals(current_index, shape){
		var index1=current_index -1*(endX - startX) + 0;
		var index2=current_index +1*(endX - startX) + 0;
		applyDot(current_index, index1, index2, shape);
		index1 = current_index - 1;
		index2 = current_index + 1;
		applyDot(current_index, index1, index2, shape);
		var infor=arrayInfoRendering[current_index];
		if(infor.infoInflectionPoints.length >0){
			var count=0, avgColor=new THREE.Color(0,0,0); 
			for(var i=0; i < infor.infoInflectionPoints.length; i++){
				var infpoint=infor.infoInflectionPoints[i];
				avgColor.r=avgColor.r+infpoint.colorInfPoint.r;
				avgColor.g=avgColor.g+infpoint.colorInfPoint.g;
				avgColor.b=avgColor.b+infpoint.colorInfPoint.b;
				count++;
			}
			avgColor.r=avgColor.r/count;
			avgColor.g=avgColor.g/count;
			avgColor.b=avgColor.b/count;
			infor.colorShape.r=avgColor.r;
			infor.colorShape.g=avgColor.g;
			infor.colorShape.b=avgColor.b;
			// blackout of color of contour
			var norm=Models.normColor(infor.colorShape);
			Models.setNewColor(infor.colorShape, norm - 0.2);
		}
	}
	
    var gaussianBlurKernel = [
      0.045, 0.122, 0.045,
      0.122, 0.332, 0.122,
      0.045, 0.122, 0.045,
    ];

	function gaussianBlur(current_index, shape){
		var kernelRED=0, kernelGREEN=0, kernelBLUE=0;
		var weight=0;
		for(var i=-1; i<2; i++){
			for(var j=-1; j <2; j++){
				var index1=current_index + i*(endX - startX) + j;
				if(index1 >= arrayInfoRendering.length || index1 <0)
					return;
				var infor1=arrayInfoRendering[index1];
				if((shape >= 0 && infor1.shape == shape) || shape <0){
					kernelRED+=infor1.colorShape.r*gaussianBlurKernel[(j+1)+(i+1)*3];
					kernelGREEN+=infor1.colorShape.g*gaussianBlurKernel[(j+1)+(i+1)*3];
					kernelBLUE+=infor1.colorShape.b*gaussianBlurKernel[(j+1)+(i+1)*3];
				}
				weight+=gaussianBlurKernel[(j+1)+(i+1)*3];
			}
		}
		var infor=arrayInfoRendering[current_index];
		infor.colorShape.r=kernelRED/(weight >1?weight:1);
		infor.colorShape.g=kernelGREEN/(weight >1?weight:1);
		infor.colorShape.b=kernelBLUE/(weight >1?weight:1);
	}
	
	

/*
		Y1 = 1.6229 +0.205*X1 +0.4005*X2 +0.342*X3 +0.3616*X4 +0.2248*X5 +0.0295*X1*X2 +0.0487*X1*X5 +0.0488*X2*X3 +
		0.0879*X4*X5 +0.0293*X1*X2*X3 -0.0684*X1*X2*X4 -0.088*X2*X3*X4 -0.1076*X2*X3*X5							
*/		
	function applyMyFilter(current_index, shape, radius_in){
		var radius =radius_in;
		var infor=arrayInfoRendering[current_index];
		var x1_f = calcX(-1, radius, infor, current_index, 0);
		var x2_f =calcX(0, radius, infor, current_index, 0);
		var x3_f =calcX(1, radius, infor, current_index, 0);
		var x4_f =calcX(0, radius, infor, current_index, 2);
		var x5_f =calcX(0, radius, infor, current_index, 4);
		var X1=Models.normFactorColor(x1_f);
		var X2=Models.normFactorColor(x2_f);
		var X3=Models.normFactorColor(x3_f);
		var X4=Models.normFactorColor(x4_f);
		var X5=Models.normFactorColor(x5_f);
		var y = 1.6229 +0.205*X1 +0.4005*X2 +0.342*X3 +0.3616*X4 +0.2248*X5 +0.0295*X1*X2 +0.0487*X1*X5 +0.0488*X2*X3 +
		0.0879*X4*X5 +0.0293*X1*X2*X3 -0.0684*X1*X2*X4 -0.088*X2*X3*X4 -0.1076*X2*X3*X5;
		y = y >3? 3 : y;
		Models.setNewColor(infor.colorShape, y);
	}

/*
		Y1 = 1.6035 +0.1855*X1 +0.42*X2 +0.3419*X3 +0.3616*X4 +0.2248*X5 +0.0488*X1*X2 +0.0877*X2*X3 +0.0293*X2*X4 -
		0.0293*X2*X5 -0.0293*X3*X5 +0.1076*X4*X5 -0.0294*X1*X2*X4 -0.0294*X1*X3*X4 -0.0488*X1*X3*X5 -0.0881*X2*X3*X5							
							
*/		
	function applyMyFilterCol(current_index, shape, radius_in){
		var radius =radius_in;
		var infor=arrayInfoRendering[current_index];
		var x1_f = calcXC(-1, radius, infor, current_index, 0);
		var x2_f =calcXC(0, radius, infor, current_index, 0);
		var x3_f =calcXC(1, radius, infor, current_index, 0);
		var x4_f =calcXC(0, radius, infor, current_index, 1);
		var x5_f =calcXC(0, radius, infor, current_index, 3);
		var X1=Models.normFactorColor(x1_f);
		var X2=Models.normFactorColor(x2_f);
		var X3=Models.normFactorColor(x3_f);
		var X4=Models.normFactorColor(x4_f);
		var X5=Models.normFactorColor(x5_f);
		var y = 1.6035 +0.1855*X1 +0.42*X2 +0.3419*X3 +0.3616*X4 +0.2248*X5 +0.0488*X1*X2 +0.0877*X2*X3 +0.0293*X2*X4 -
		0.0293*X2*X5 -0.0293*X3*X5 +0.1076*X4*X5 -0.0294*X1*X2*X4 -0.0294*X1*X3*X4 -0.0488*X1*X3*X5 -0.0881*X2*X3*X5;
		y = y >3 ? 3 : y;
		Models.setNewColor(infor.colorShape, y);
	}



/*
							
		Y=1.9702+0.03418*X1+0.0343*X3+0.11242*X4+0.18098*X5-0.2102*X6-0.0343*X1*X3+0.17098*X1*X6+0.0243*X2*X3+0.06336*X2*X4+0.1516*X2*X6+0.06355*X3*X4+0.03406*X3*X5+0.21*X3*X6+
		0.05375*X4*X5+0.22922*X4*X6+0.25871*X5*X6+0.03398*X1*X2*X3+0.0341*X1*X3*X4-0.03422*X1*X3*X6-0.05359*X1*X4*X6-0.04387*X1*X5*X6+0.04387*X3*X4*X5-
		0.03418*X3*X4*X6-0.0243*X3*X5*X6-0.02477*X4*X5*X6							
														
*/	
	
	function applyMyFilterContour(current_index, shape, radius_in, infpoint){
		var radius =radius_in;
		var infor=arrayInfoRendering[current_index];
		var x1_f = calcXContour(-1, radius, infor, current_index, 0);
		var x2_f =calcXContour(0, radius, infor, current_index, 0);
		var x3_f =calcXContour(1, radius, infor, current_index, 0);
		var x4_f =calcXContour(-2, radius, infor, current_index, 0);
		var x5_f =calcXContour(2, radius, infor, current_index, 0);
		var x6_f=infpoint.sinAngle;
		var X1=Models.normFactorColor(x1_f);
		var X2=Models.normFactorColor(x2_f);
		var X3=Models.normFactorColor(x3_f);
		var X4=Models.normFactorColor(x4_f);
		var X5=Models.normFactorColor(x5_f);
		var X6=Models.normFactorCross(x6_f);
		var y=1.8702+0.03418*X1+0.0343*X3+0.11242*X4+0.18098*X5-0.2102*X6-0.0343*X1*X3+0.17098*X1*X6+0.0243*X2*X3+0.06336*X2*X4+0.1516*X2*X6+0.06355*X3*X4+0.03406*X3*X5+0.21*X3*X6+
		0.05375*X4*X5+0.22922*X4*X6+0.25871*X5*X6+0.03398*X1*X2*X3+0.0341*X1*X3*X4-0.03422*X1*X3*X6-0.05359*X1*X4*X6-0.04387*X1*X5*X6+0.04387*X3*X4*X5-
		0.03418*X3*X4*X6-0.0243*X3*X5*X6-0.02477*X4*X5*X6	;	
		infpoint.colorInfPoint.r=infor.colorShape.r;
		infpoint.colorInfPoint.g=infor.colorShape.g;
		infpoint.colorInfPoint.b=infor.colorShape.b;
		y = y > 3 ? 3 : y;
		Models.setNewColor(infpoint.colorInfPoint, y);
	}
	
	function calcX(index_row, radius_in, infor, current_index, prev){
		var avgNorm=Models.normColor(infor.colorShape);
		var radius=radius_in;
		var count=1;
		var tmpInfor=infor;
		for(var j= -1; j > -radius -1 ; j--){
			var index1=(current_index - prev) + index_row*(endX - startX) + j;
			if(index1>= arrayInfoRendering.length || index1 < 0)
				break;
			var infor1=arrayInfoRendering[index1];
			var dist= Models.distance(tmpInfor.point, infor1.point);
			if((dist < 0.09 && tmpInfor.onContour == false && tmpInfor.shape==infor.shape)){
				avgNorm=avgNorm+Models.normColor(infor1.colorShape);
				count++;
			}
			else
				break;
			tmpInfor=infor1;
		}
		tmpInfor=infor;
		for(var j= 1; j <radius +1 ; j++){
			var index1=(current_index - prev) + index_row*(endX - startX) + j;
			if(index1>= arrayInfoRendering.length || index1 < 0)
				break;
			var infor1=arrayInfoRendering[index1];
			var dist= Models.distance(tmpInfor.point, infor1.point);
			if((dist < 0.09 && tmpInfor.onContour == false && tmpInfor.shape==infor.shape)){
				avgNorm=avgNorm+Models.normColor(infor1.colorShape);
				count++;
			}
			else
				break;
			tmpInfor=infor1;
		}
		avgNorm=avgNorm/count;
		if(count == 1   )
			avgNorm = avgNorm < 2.0 ? avgNorm : 2.0;
		return avgNorm;
	}
	
	function calcXContour(index_row, radius_in, infor, current_index, prev){
		var avgNorm=Models.normColor(infor.colorShape);
		var radius=radius_in;
		var count=1;
		var tmpInfor=infor;
		for(var j= -1; j > -radius -1 ; j--){
			var index1=(current_index - prev) + index_row*(endX - startX) + j;
			if(index1>= arrayInfoRendering.length || index1 < 0)
				break;
			var infor1=arrayInfoRendering[index1];
			var dist= Models.distance(tmpInfor.point, infor1.point);
			if((dist < 0.09 && tmpInfor.shape==infor.shape)){
				avgNorm=avgNorm+Models.normColor(infor1.colorShape);
				count++;
			}
			else
				break;
			tmpInfor=infor1;
		}
		tmpInfor=infor;
		for(var j= 1; j <radius +1 ; j++){
			var index1=(current_index - prev) + index_row*(endX - startX) + j;
			if(index1>= arrayInfoRendering.length || index1 < 0)
				break;
			var infor1=arrayInfoRendering[index1];
			var dist= Models.distance(tmpInfor.point, infor1.point);
			if((dist < 0.09 && tmpInfor.shape==infor.shape)){
				avgNorm=avgNorm+Models.normColor(infor1.colorShape);
				count++;
			}
			else
				break;
			tmpInfor=infor1;
		}
		avgNorm=avgNorm/count;
		return avgNorm;
	}

	function calcXC(index_col, radius_in, infor, current_index, prev){
		var avgNorm=Models.normColor(infor.colorShape);
		var radius=radius_in;
		var count=1;
		var tmpInfor=infor;
		for(var j= -1; j > -radius-1; j--){
			var index1=(current_index - prev) + j*(endX - startX) + index_col;
			if(index1>= arrayInfoRendering.length || index1 < 0)
				break;
			var infor1=arrayInfoRendering[index1];
			var dist= Models.distance(tmpInfor.point, infor1.point);
			if(dist < 0.09 && tmpInfor.onContour == false && tmpInfor.shape==infor.shape){
				avgNorm=avgNorm+Models.normColor(infor1.colorShape);
				count++;
			}
			else
				break;
			tmpInfor=infor1;
		}
		tmpInfor=infor;
		for(var j= 1; j < radius+1; j++){
			var index1=(current_index - prev) + j*(endX - startX) + index_col;
			if(index1>= arrayInfoRendering.length || index1 < 0)
				break;
			var infor1=arrayInfoRendering[index1];
			var dist= Models.distance(tmpInfor.point, infor1.point);
			if(dist < 0.09 && tmpInfor.onContour == false && tmpInfor.shape==infor.shape){
				avgNorm=avgNorm+Models.normColor(infor1.colorShape);
				count++;
			}
			else
				break;
			tmpInfor=infor1;
		}
		avgNorm=avgNorm/count;
		return avgNorm;
	}

	function drawAddCanvas(){
		gl_add.canvas.width = SCREEN_WIDTH;
		gl_add.canvas.height = SCREEN_HEIGHT;
		setSimpleShader(gl_add, shaderSimple);
		camera.updateProjectionMatrix();
		data.projectionMatrix.copy(camera.projectionMatrix);
		for(var i=0; i < arrayInfoRendering.length; i++){
			var infor=arrayInfoRendering[i];
			var inforBottom, inforBottomLeft, inforBottomRight;
			var inforTop, inforTopLeft, inforTopRight;
			var index = i - 1*((endX - startX)) +0;
			if(index >= 0 && index < arrayInfoRendering.length)
				inforTop=arrayInfoRendering[index];
			else
				inforTop=infor;
			index = i - 1*((endX - startX)) -1;
			if(index >= 0 && index < arrayInfoRendering.length && infor.xShape >= startX +5)
				inforTopLeft=arrayInfoRendering[index];
			else
				inforTopLeft=infor;
			index = i - 1*((endX - startX)) +1;
			if(index >= 0 && index < arrayInfoRendering.length && infor.xShape <= endX -5)
				inforTopRight=arrayInfoRendering[index];
			else
				inforTopRight=infor;
			index = i + 1*((endX - startX)) +0;
			if(index >= 0 && index < arrayInfoRendering.length)
				inforBottom=arrayInfoRendering[index];
			else
				inforBottom=infor;
			index = i + 1*((endX - startX)) -1;
			if(index >= 0 && index < arrayInfoRendering.length && infor.xShape >= startX + 5)
				inforBottomLeft=arrayInfoRendering[index];
			else
				inforBottomLeft=infor;
			index = i + 1*((endX - startX)) +1;
			if(index >= 0 && index < arrayInfoRendering.length && infor.xShape <= endX -5)
				inforBottomRight=arrayInfoRendering[index];
			else
				inforBottomRight=infor;
			if(infor.shape == 0)
				data.modelViewMatrix.copy(camera.matrixWorldInverse);
			if(infor.shape == 1)
				data.modelViewMatrix.copy(camera.matrixWorldInverse);
			if(infor.shape == 2)
				data.modelViewMatrix.copy(camera.matrixWorldInverse);
			data.colorBottom.r=inforBottom.colorShape.r;
			data.colorBottom.g=inforBottom.colorShape.g;
			data.colorBottom.b=inforBottom.colorShape.b;
			data.colorBottomLeft.r=inforBottomLeft.colorShape.r;
			data.colorBottomLeft.g=inforBottomLeft.colorShape.g;
			data.colorBottomLeft.b=inforBottomLeft.colorShape.b;
			data.colorBottomRight.r=inforBottomRight.colorShape.r;
			data.colorBottomRight.g=inforBottomRight.colorShape.g;
			data.colorBottomRight.b=inforBottomRight.colorShape.b;
			data.colorTop.r=inforTop.colorShape.r;
			data.colorTop.g=inforTop.colorShape.g;
			data.colorTop.b=inforTop.colorShape.b;
			data.colorTopLeft.r=inforTopLeft.colorShape.r;
			data.colorTopLeft.g=inforTopLeft.colorShape.g;
			data.colorTopLeft.b=inforTopLeft.colorShape.b;
			data.colorTopRight.r=inforTopRight.colorShape.r;
			data.colorTopRight.g=inforTopRight.colorShape.g;
			data.colorTopRight.b=inforTopRight.colorShape.b;
			var delta=0.025, deltaZZ=0.001; 
			var deltaX, deltaY, deltaZ;
			deltaX= delta;
			deltaY= delta;
			deltaZ= deltaZZ;
			set3DRectanglePoint(infor.point, infor.uvCoordinates, deltaX, deltaY, deltaZ);
			setColorsRectangle(infor.colorShape,  infor.shape, 6, data.colors);
			runSimpleShader(gl_add, data, gl_add.TRIANGLES, 3);
			deltaX= -delta;
			deltaY= -delta;
			deltaZ= -deltaZZ;
			set3DRectanglePoint(infor.point, infor.uvCoordinates, deltaX, deltaY, deltaZ);
			setColorsRectangle(infor.colorShape, infor.shape, 6, data.colors);
			runSimpleShader(gl_add, data, gl_add.TRIANGLES, 3);
			deltaX= delta;
			deltaY= -delta;
			deltaZ= deltaZZ;
			set3DRectanglePoint(infor.point, infor.uvCoordinates, deltaX, deltaY, deltaZ);
			setColorsRectangle(infor.colorShape,  infor.shape, 6, data.colors);
			runSimpleShader(gl_add, data, gl_add.TRIANGLES, 3);
			deltaX= -delta;
			deltaY= delta;
			deltaZ= deltaZZ;
			set3DRectanglePoint(infor.point, infor.uvCoordinates, deltaX, deltaY, deltaZ);
			setColorsRectangle(infor.colorShape, infor.shape, 6, data.colors);
			runSimpleShader(gl_add, data, gl_add.TRIANGLES, 3);
			deltaX= delta;
			deltaY= -delta;
			deltaZ= -deltaZZ;
			set3DRectanglePoint(infor.point, infor.uvCoordinates, deltaX, deltaY, deltaZ);
			setColorsRectangle(infor.colorShape,  infor.shape, 6, data.colors);
			runSimpleShader(gl_add, data, gl_add.TRIANGLES, 3);
			deltaX= -delta;
			deltaY= delta;
			deltaZ= -deltaZZ;
			set3DRectanglePoint(infor.point, infor.uvCoordinates, deltaX, deltaY, deltaZ);
			setColorsRectangle(infor.colorShape,  infor.shape, 6, data.colors);
			runSimpleShader(gl_add, data, gl_add.TRIANGLES, 3);
			deltaX= -delta;
			deltaY= -delta;
			deltaZ= deltaZZ;
			set3DRectanglePoint(infor.point, infor.uvCoordinates, deltaX, deltaY, deltaZ);
			setColorsRectangle(infor.colorShape,  infor.shape, 6, data.colors);
			runSimpleShader(gl_add, data, gl_add.TRIANGLES, 3);
			deltaX= delta;
			deltaY= delta;
			deltaZ= -deltaZZ;
			set3DRectanglePoint(infor.point, infor.uvCoordinates, deltaX, deltaY, deltaZ);
			setColorsRectangle(infor.colorShape,  infor.shape, 6, data.colors);
			runSimpleShader(gl_add, data, gl_add.TRIANGLES, 3);
		}
	}
	
	
	function setRectangle(x, y, width, height){
		var x1=x, x2=x+width;
		var y1=y, y2=y+height;
		x1=(((x1 ) - SCREEN_WIDTH/2)*2)/SCREEN_WIDTH;
		x2=(((x2) - SCREEN_WIDTH/2)*2)/SCREEN_WIDTH;
		y1=((SCREEN_HEIGHT/2 - (y1))*2)/SCREEN_HEIGHT;
		y2=((SCREEN_HEIGHT/2 - (y2))*2)/SCREEN_HEIGHT;
		data.positions.splice(0, data.positions.length);
		data.positions=[x1, y1,
		 x1, y2,
		 x2, y1,
		 x2, y2,
		 x2, y1,
		 x1, y2
		 ];
	}
	
	function set3DRectanglePoint(point, uvCoordinates, width, height, depth){
		data.positions.splice(0, data.positions.length);
		data.texcoords.splice(0, data.texcoords.length);
		var x1=point.x;
		var x2=point.x+width;
		var y1=point.y;
		var y2=point.y+height;
		var z1=point.z;
		var z2=point.z+depth;
		var u1=uvCoordinates.x;
		var v1=uvCoordinates.y;
		var u2=uvCoordinates.x + width*0.1;
		var v2=uvCoordinates.y + height*0.1;
		data.positions=[
		 x1, y1, z1,
		 x1, y2, z2,
		 x1, y1, z2,
//--------------------------		 
		 x1, y1, z2,
		 x1, y2, z2,
		 x1, y2, z1,
//==========================		 
		 x1, y1, z1,
		 x2, y2, z1,
		 x1, y2, z2, 
//--------------------------		 
		 x2, y2, z1, 
		 x2, y1, z1,
		 x1, y2, z2,
//==========================
		 x2, y2, z1,
		 x1, y1, z2,
		 x1, y1, z1,
//--------------------------
		 x2, y2, z1,
		 x2, y1, z2,
		 x1, y1, z2,
//==========================
		 x2, y1, z2,
		 x1, y2, z1,
		 x1, y1, z2,
//--------------------------
		 x2, y1, z2,
		 x2, y2, z2,
		 x1, y2, z1,
//==========================
		 x2, y2, z1,
		 x2, y1, z1,
		 x2, y2, z2,
//-------------------------
		 x2, y2, z1,
		 x2, y2, z2,
		 x2, y1, z2,
//==========================
		 x1, y2, z2,
		 x2, y1, z1, 
		 x1, y2, z1,
//-------------------------
		 x2, y1, z1,
		 x2, y2, z2,
		 x1, y2, z1
		 ];
		data.texcoords = [
		 u1, v1, 
		 u1, v2, 
		 u1, v1, 
//--------------------------		 
		 u1, v1, 
		 u1, v2, 
		 u1, v2, 
//==========================		 
		 u1, v1, 
		 u2, v2, 
		 u1, v2,  
//--------------------------		 
		 u2, v2,  
		 u2, v1, 
		 u1, v2, 
//==========================
		 u2, v2, 
		 u1, v1, 
		 u1, v1, 
//--------------------------
		 u2, v2, 
		 u2, v1, 
		 u1, v1, 
//==========================
		 u2, v1, 
		 u1, v2, 
		 u1, v1, 
//--------------------------
		 u2, v1, 
		 u2, v2, 
		 u1, v2, 
//==========================
		 u2, v2, 
		 u2, v1, 
		 u2, v2, 
//-------------------------
		 u2, v2, 
		 u2, v2, 
		 u2, v1, 
//==========================
		 u1, v2, 
		 u2, v1,  
		 u1, v2, 
//-------------------------
		 u2, v1, 
		 u2, v2, 
		 u1, v2
		 
		 ]
	}
	
	function setColorPoint(pixelColor, shape, index){
		if(index ==0)
			data.colors.splice(0, data.colors.length);
		data.colors[index]=pixelColor.r*255;
		data.colors[index+1]=pixelColor.g*255;
		data.colors[index+2]=pixelColor.b*255;
		if(shape ==0)
			data.colors[index+3]=255;
		if(shape == 1)
			data.colors[index+3]=1*255;
		if(shape == 2)
			data.colors[index+3]=1.0*255;
	}
	
	function setColorsRectangle(pixelColor, shape, count_face, store){
		store.splice(0, store.length);
		var index = 0;
		for(var i=0; i<count_face; i++){
			store[index]=pixelColor.r*255;
			store[index+1]=pixelColor.g*255;
			store[index+2]=pixelColor.b*255;
			if(shape ==0)
				store[index+3]=255;
			if(shape == 1)
				store[index+3]=1*255;
			if(shape == 2)
				store[index+3]=1.0*255;
			index+=4;
			store[index]=pixelColor.r*255;
			store[index+1]=pixelColor.g*255;
			store[index+2]=pixelColor.b*255;
			if(shape ==0)
				store[index+3]=255;
			if(shape == 1)
				store[index+3]=1*255;
			if(shape == 2)
				store[index+3]=1.0*255;
			index+=4;
			store[index]=pixelColor.r*255;
			store[index+1]=pixelColor.g*255;
			store[index+2]=pixelColor.b*255;
			if(shape ==0)
				store[index+3]=255;
			if(shape == 1)
				store[index+3]=1*255;
			if(shape == 2)
				store[index+3]=1.0*255;
			index+=4;
			store[index]=pixelColor.r*255;
			store[index+1]=pixelColor.g*255;
			store[index+2]=pixelColor.b*255;
			if(shape ==0)
				store[index+3]=255;
			if(shape == 1)
				store[index+3]=1*255;
			if(shape == 2)
				store[index+3]=1.0*255;
			index+=4;
			store[index]=pixelColor.r*255;
			store[index+1]=pixelColor.g*255;
			store[index+2]=pixelColor.b*255;
			if(shape ==0)
				store[index+3]=255;
			if(shape == 1)
				store[index+3]=1*255;
			if(shape == 2)
				store[index+3]=1.0*255;
			index+=4;
			store[index]=pixelColor.r*255;
			store[index+1]=pixelColor.g*255;
			store[index+2]=pixelColor.b*255;
			if(shape ==0)
				store[index+3]=255;
			if(shape == 1)
				store[index+3]=1*255;
			if(shape == 2)
				store[index+3]=1.0*255;
			index+=4;
		}
	}
	

};
Object.assign( THREE.WEBGLRaytracingRenderer.prototype, THREE.EventDispatcher.prototype );
